# Segmentation Notes

Segmentation files are stored in the `/backend/placenta/segmentation` directory with individual methods and trained models in `/backend/placenta/segmentation/internal`.

Segmentation can take time depending on method used.  If you trigger a segmentation and want to leave the webpage, the server will carry out the segmentation as long as it is still running.

Also, the methods included here may not be as optimal as the method versions in the segmentation demo, as the methods used here may be slightly altered for running in the integration environment.  Auto-segmented masks will appear somewhat pixelly as they are generated at 50% resolution for speed, and then rescaled.