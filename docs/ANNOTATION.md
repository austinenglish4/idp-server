# Annotation Application Interface
Sample Usage
![Sample Usage](./assets/annotation_sample.gif)

The application has two pages:

Browse
![Browse View](./assets/annotation_browse.jpg)

and Annotate
![Annotate View](./assets/annotation_annotate.jpg)

In the Browse View, you can:

Toggle on and off mask overlays with the visibility icon (eye) in an image's details overlay, navigate to the Annotate View by clicking the image edit icon (pencil), or delete an image by clicking the trash can icon.
![Mask Overlays](./assets/annotation_browse_details_closed.jpg)

You can also trigger one of the available segmentation methods.  This will run segmentation which can take more or less time depending on the method, and when segmentation completes, existing masks will be removed and replaced by segmentation results.  Segmentation can be triggered and the web page navigated away from.  The server will continue to generate results even though the UI will no longer show loading status if returning to the page.
![Browse](./assets/annotation_browse_details.jpg)

In the Annotate View, the left sidenav contains view options and editing controls.  From top to bottom in the sidenav, you can:

1. Adjust image zoom
2. Add new masks
3. Toggle a mask's visibility
4. Change a mask's view color (this does not persist in the database or correlate to labels)
5. Select a mask for editing
6. Delete a mask
7. Select the Draw Tool
8. Select the Polygon Tool
9. Select the Add mode
10. Select the Subtract mode
11. Adjust mask overlay opacity
12. Adjust brush size
13. Merge a mask with the currently edited one
14. Discard edits on an active mask
15. Save edits on active mask
16. Toggle a reference image view for use when also viewing masks on the main image

![Annotate Details](./assets/annotation_annotate_sidenav_numbered.jpg)

The Draw Tool is simple to use.  With a mouse or stylus, click and drag or tap and drag on the image while in editing mode for a mask.

The Polygon Tool works by either clicking on polygon corners to draw the polygon or drawing the polygon edges with a stylus.  Double-clicking will complete the polygon, or more easily, clicking the Fill button will as well.
![Annotate Details](./assets/annotation_annotate_fill_circled.jpg)

An edited mask will always turn white. When finished editing an image, you can return to the Browse View by clicking Browse in the top left of the sidenav.
Additionally, you can edit a mask's name while the mask is in edit mode by clicking on the name, entering a new one and saving the mask.
![Edit name](./assets/annotation_annotate_name.jpg)




