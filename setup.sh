#! usr/bin/bash

echo "Applying Migrations"
(cd backend && python manage.py migrate)
echo "Clearing Database"
(cd backend && python manage.py clear)
echo "Initializing Database"
(cd backend && python manage.py initialize)
echo "Collect Static Files"
(cd backend && python manage.py collectstatic --noinput)
