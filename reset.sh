echo "Clearing Database"
(cd backend && python manage.py clear)

echo "Initializing Database"
(cd backend && python manage.py clear)

echo "Collecting Static Files"
(cd backend && python manage.py collectstatic --noinput)