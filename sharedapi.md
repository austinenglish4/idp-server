# Spectral images

*Get list of all spectral images within the database.*

### /api/spims

**GET**

Returns:

    {
        "spims": [
            {
                "name": "Set 1, blue dye",
                "pk": 413,
                "rgb": "/files/rgb/Set 1, blue dye.png"
            }
        ]
    }


# Spectral image

*Get specific spectral image. PK is of spectral image.*

### /api/spim/**int:pk**/

**GET**

Returns:

    {
        "name": "Set 1, blue dye",
        "pk": 413,
        "rgb": "/files/rgb/Set 1, blue dye.png",
        "spim_cube": "placenta/files/hdf5_files/Set 1, blue dye.hdf5",
        "annotation_id": 413,
        "masks": [
            {
                "pk": 2000,
                "bitmap": "/files/masks/Set 1, blue dye - Blue dye.png",
                "name": "Blue dye",
                "tissueId": 401,
                "classId": 401
            }
        ]
    }


# Upload automatic segmentation results

*Upload masks from the automatic segmentation. PK is of spectral image.*

### /api/spim/**int:pk**/update/

**POST**

Body:

    BINARY mask.png 


# Upload new mask

*Upload single mask from annotation tool. PK is of spectral image.*

### /api/spim/**int:pk**/createmask/

**POST**

Body:
    
    BINARY mask.png 


# Update existing mask

*Replace single existing mask by annotation tool result. PK is of mask.*

### /api/mask/**int:pk**/update/

**POST**

Body:

    {
        "hdf5path": "path/to/the/file.h5",
    }


# TissueClass

*Get all tissueclasses. Not necessary?*

### /api/tissueclass/

**GET**

Returns:

    {
        "TissueClass": [
            {
                "tissue_type": "Tissue type #1",
                "class_id": 401,
                "pk": 401
            }
        ]
    }


# ClassFeature

*Get all classfeatures. Not necessary?*

### /api/classfeature/

**GET**

Returns:

    {
        "ClassFeature": [
            {
                "class_feature": "Class feature #1",
                "pk": 401
            }
        ]
    }

# Diagnosis

*Get all diagnoses. Not necessary?*

### /api/diagnosis/

**GET**

Returns:

    {
        "Diagnosis": [
            {
                "diagnosis_type": "Diagnosis type #1",
                "level": "Diagnosis #1 level",
                "description": "Diagnosis #1 description",
                "pk": 398
            }
        ]
    }
