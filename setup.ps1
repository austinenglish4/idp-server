Write-Output "Applying Migrations"
$cmd = 'python'
$args = 'manage.py', 'migrate'
$process = Start-Process $cmd $args -Wait -WorkingDirectory "backend"  -NoNewWindow

Write-Output "Clearing Database"
$cmd = 'python'
$args = 'manage.py', 'clear'
$process = Start-Process $cmd $args -Wait -WorkingDirectory "backend"  -NoNewWindow

Write-Output "Initializing Database"
$cmd = 'python'
$args = 'manage.py', 'initialize'
$process = Start-Process $cmd $args -Wait -WorkingDirectory "backend"  -NoNewWindow

Write-Output "Collecting Static Files"
$cmd = 'python'
$args = 'manage.py', 'collectstatic', '--noinput'
$process = Start-Process $cmd $args -Wait -WorkingDirectory "backend"  -NoNewWindow
