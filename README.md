# Readme

## Integrated systems
* Django Server and Database
    * sourced from Group 1 and modified to integrate
* Static web application under /annotate directory
    * annotation app built in Angular and served from Django server
* Auto-segmentation code under /backend/placenta/segmentation
    * 5 automatic segmentation methods, able to be run from the annotation app UI

## Setup and run:

1. Install or use **Python 3.10.8** (this specific version was used to develop, 3.10.x may work as well).

2. Install and activate some **virtual environment for python** like Anaconda or virtualenv
```
# virtualenv example: Linux bash

python -m venv venv
source ./venv/bin/activate
```

```
# virtualenv example: Windows Powershell

python -m venv venv
./venv/Scripts/activate
```

3. With the virtual environment activated and `python` available in the shell as a command, install python dependencies:

```
pip install -r requirements.txt
```

4. Next, initialize the database with some existing spectral tiffs and associated mask tiffs (masks are not required).  Either copy/paste spectral tiffs and mask tiffs into project directories `Placenta HSI/data/flat-fielded` and `Placenta HSI/data/binary-masks` respectively or set `INPUT_DATA_FOLDER` in `backend\.env` to an absolute system path to a single directory containing folders `data/flat-fielded` and `data/binary-masks` on your machine.  Only files with a '.tif' or '.tiff' extension are read. Note that the current database design creates hdf5 format copies of the spectral image data for reference and so disk space proportional to the number of loaded spectral images will be required.

5.  Finally, run the following setup and run scripts depending on your OS.  For Windows Powershell, it is necessary to have ExecutionPolicy set to RemoteSigned which must be set from an Admin Powershell instance (Run as Administrator).  Alternately, run the python commands below in order, from the `/backend` directory.

```
# Windows with Powershell

Set-ExecutionPolicy RemoteSigned

./setup
./run
```
or
```
# Linux with bash
bash ./setup.sh
bash ./run.sh
```
or
```
# setup

cd backend

python manage.py migrate
python manage.py clear
python manage.py initialize
python manage.py collectstatic --noinput

# run
python manage.py runserver
```

4. The Django server runs on port 8000 and the annotation app is served from the base path at http://localhost:8000 (other paths may direct to other team's views).

5. If desired, reset the database and reload your spectral tiffs using the reset scripts (spectral tiffs and masks will be loaded in again after clearing database).

```
# Windows with Powershell
./reset
```
or
```
# Linux with bash
bash ./reset.sh
```
or
```
# reset

cd backend
python manage.py clear
python manage.py initialize
python manage.py collectstatic --noinput
```

6. See [SEGMENTATION_NOTES.md](./docs/SEGMENTATION_NOTES.md) for notes about the segmentation methods included in this spectral image annotation system and [ANNOTATION.md](./docs/ANNOTATION.md) for some notes on the annotation application interface.

Notes:
* If, due to a bug, the database entities become corrupted or unusable, consider running the reset script or reset sequence of commands, running the server again, and trying the same annotation operations again.
* Development testing was done in Firefox 107.0.1 but most up-to-date modern browsers should work for running the app
