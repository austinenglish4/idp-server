from django.shortcuts import render

def handler400(request, exception):
    print(exception)
    return render(request, "400.html", {'exception': exception}, status=400)

def handler403(request, exception):
    print(exception)
    return render(request, "403.html", {'exception': exception}, status=403)

def handler404(request, exception):
    print(exception)
    return render(request, "404.html", {'exception': exception}, status=404)

def handler500(request):
    return render(request, "500.html", status=500)