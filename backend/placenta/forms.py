from django import forms 

class UploadFileForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

class UploadSpimForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True, 'accept': '.tif, .tiff'}))

class UploadAnnotationForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': False, 'accept': '.csv, .tif, .tiff'}))

class UploadMaskForm(forms.Form):
    file = forms.FileField(required=False, widget=forms.ClearableFileInput(attrs={'multiple': False, 'accept': '.png'}))
