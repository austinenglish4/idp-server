import os
import random
import json
import string
import pathlib
import h5py
import numpy as np
import imageio
from PIL import Image as im
from datetime import datetime, timezone
from django.shortcuts import redirect, render
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from django.http import HttpResponse
from django.http import FileResponse
from django.http import JsonResponse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from placenta.models import SpectralImage, Mask, Annotation, IndividualMask, TissueClass, ClassFeature, Diagnosis
from .forms import UploadFileForm, UploadSpimForm, UploadAnnotationForm, UploadMaskForm
from utils.annotations import *
from utils.spectral_tiffs import *
from dotenv import load_dotenv
import uuid

def create_mask_instance(flattened, filename, pk):

    if filename[-4:] == '.csv': # from csv
        filename = filename[:-4]
    else: # from tif
        filename = filename[:-10]
    
    # Get reference to spim object via pk
    spectral_image = SpectralImage.objects.get(pk=pk)
        
    # Get reference to Annotation object
    annotation = Annotation.objects.get(spim_id = spectral_image)
    
    # For each mask in annotation
    for mask in flattened:
        # Create png of the mask
        png = np.reshape(flattened[mask], (
            flattened[mask].shape[0],
            flattened[mask].shape[1]
        ))
        png_data = im.fromarray(png).convert('L')
        rgba = png_data
        rgba.putalpha(im.fromarray(png))
        png_path = './placenta/files/masks/' + str(uuid.uuid4()) + '.png'
        png_data.save(png_path)
        
        # Create mask object
        IndividualMask.objects.create(
            annotation_id = annotation,
            path_to_png = png_path[10:],
            label = str(mask),
            color_hex = '%02x%02x%02x' % (random.randint(0,255), random.randint(0,255), random.randint(0,255))
        )


@api_view(['GET'])
def home(request):
    """
    Home page
    """
    
    return render(request, 'placenta/home.html', {})

@api_view(['GET'])
def get_band(request, pk, index):
    """
    /spim/<str:pk>/bands/<str:index>    GET     get one band
    """
    
    if request.method == 'GET':
        with h5py.File(SpectralImage.objects.get(pk=pk).path_to_hdf5,'r') as specImg:
            wavelength = np.array(specImg['spim'])
        wavelengthData = []
        imgWidth = wavelength[0].size/wavelength[0][0].size
        imgHeight = wavelength.size/wavelength[0].size
        for row in range(0,int(imgHeight)):
            for column in range(0,int(imgWidth)):
                wavelengthData.append(wavelength[row][column][int(index)])
        return HttpResponse(json.dumps({"band":str(wavelengthData),"width":imgWidth,"height":imgHeight}),content_type='application/json')


@api_view(['GET', 'POST'])
def spectral_images(request):
    """
    /placenta/spims/    GET      get all spectral images
    /placenta/spims/    POST     create new spectral image
    """

    if request.method == 'GET':

        # Get data for the gallery view
        form = UploadSpimForm()
        spims = SpectralImage.objects.all().values_list('pk', 'label', 'path_to_thumbnail')
        l1 = list(spims)
        l2 = []
        for i in l1:
            l2.append({
                'pk': i[0],
                'file_name': i[1], 
                'img_name': i[2],
            })
        
        # Render gallery view
        return render(request, 'placenta/spims.html', {
            'list': l2, 
            'form': form
        })

    elif request.method == 'POST':
        
        form = UploadFileForm(request.POST, request.FILES)
        for f in request.FILES.getlist('file'):

            name = str(f)
            # get spectral image cube
            try:
                spim_tuple = read_stiff(f)
            except:
                return render(request, 'placenta/error.html', {'error_message': 'Invalid spectral image!'})

            # create temporary tif file to read time & size, not a smart solution (?)
            templocation = './placenta/files/temp/temp.tif'
            write_stiff(
                templocation,
                spim_tuple[0],
                spim_tuple[1],
                spim_tuple[2],
                spim_tuple[3]
            )
            time = os.path.getctime(templocation)
            time = datetime.fromtimestamp(time, timezone.utc)
            time = time.astimezone().isoformat()
            size = os.path.getsize(templocation)

            # Write to hdf5 file
            hdf5_path = 'placenta/files/hdf5_files/' + str(uuid.uuid4()) + 'hdf5'
            with h5py.File(hdf5_path, 'w') as f:
                f.create_dataset('spim', spim_tuple[0].shape, data=spim_tuple[0])
                f.create_dataset('wavelengths', spim_tuple[1].shape, data=spim_tuple[1])
                f.create_dataset('rgb', spim_tuple[2].shape, data=spim_tuple[2])

            # Create thumbnail png's
            tn = np.reshape(spim_tuple[2], (
                spim_tuple[2].shape[0], # 1024
                spim_tuple[2].shape[1], # 1024
                spim_tuple[2].shape[2]  # 3
            ))
            tn_data = im.fromarray(tn)
            png_path = './placenta/files/rgb/'+ str(uuid.uuid4()) + '.png'
            tn_data.save(png_path)
            tn_data = tn_data.resize((128, 128), im.ANTIALIAS)
            tn_path = './placenta/files/thumbnails/'+ str(uuid.uuid4()) + '.png'
            tn_data.save(tn_path)

            spim = SpectralImage.objects.create(
                label=name[:-4],
                path_to_hdf5=hdf5_path,
                path_to_png = png_path[10:],
                path_to_thumbnail=tn_path[10:],
                spim_cube_metadata=spim_tuple[3],

                patient_id=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                diagnosis=(''.join(random.choice(string.ascii_letters + ' ') for i in range(25))),
                image_identifier=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                image_series_identifier=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                hsi_camera_name='Senop HSC-2',
                optic_imaging_system='Pentero 900',
                hsi_adapter_name=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                environment_description=(['OR301', 'OR302', 'OR303'][random.randint(0,2)]),
                reference=(['reflectance', 'absobance'][random.randint(0,1)]),
                light_source_description=(['Xenon', 'Blue400', 'Yellow500'][random.randint(0,2)]),
                hsi_software_name='Senop',
                hsi_software_version=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                acquisition_type=(['snapshot', 'video'][random.randint(0,1)]),
                hsi_sequence_name=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                hsi_sequence_version=(''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))),
                timestamp=time,
                data_size=size,
                frames=spim_tuple[1].shape[0],
                imaging_light_intensity=random.random(),
                imaging_magnification=random.random(),
                imaging_focal_length=random.random(),
                notes=(''.join(random.choice(string.ascii_letters + '') for i in range(50))),
            )
            # Initialize Annotation instance for created spim
            Annotation.objects.create(
                label = name[:-4],
                spim_id = spim
            )

        return(redirect('./'))
    
    return (redirect(home))


@api_view(['GET', 'POST'])
def spectral_images_list(request):
    """
    /placenta/spims/    GET      get all spectral images
    /placenta/spims/    POST     create new spectral image
    """

    if request.method == 'GET':

        # Get data for the list view
        form = UploadSpimForm()
        l1 = list(SpectralImage.objects.all())
        l2 = []
        for i in l1:
            l2.append({
                'pk': i.pk,
                'file_name': i.label, 
                'img_name': i.path_to_thumbnail,
                'metadata': i.spim_cube_metadata,
                'date': i.timestamp,
                'size': i.data_size
            })
        
        # Render list view
        return render(request, 'placenta/spims_details.html', {
            'list': l2, 
            'form': form
        })
    
    return (redirect(home))


@api_view(['GET', 'POST'])
def spectral_image(request, pk):
    """
    /placenta/spim/:id/      GET        get specific spectral image
    /placenta/spim/:id/      POST       DELETE specific spectral image
    """
    
    if request.method == 'GET': # Render spim page

        annform = UploadAnnotationForm()
        maskform = UploadMaskForm()

        spim = SpectralImage.objects.get(pk=pk)
        with h5py.File(spim.path_to_hdf5, 'r') as f:
            numpy_spim = np.array(f['spim'])
            numpy_wavelengths = np.array(f['wavelengths'])
            numpy_rgb = np.array(f['rgb'])

        imageData = numpy_rgb
        rgbImage = []
        for item in imageData:
            for item in item:
                rgbImage.extend(item)
        
        item = {
            'file_name': spim.label,
            'rgb_image_data': rgbImage,
            'image_width': imageData[0].size,
            'image_height': imageData[1].size,
            'spim': numpy_spim,
            'wavelengths': numpy_wavelengths,
            'rgb': numpy_rgb,
            'id' : pk,
            'wavelengths_size' : numpy_wavelengths.size,
        }

        # Old legacy mask object, to be deleted
        if Mask.objects.filter(pk=pk).exists():
            mask = Mask.objects.get(pk=pk)
            mask_name = mask.file_name
            csv = mask.csv_data
            item['mask_name'] = mask_name
            item['csv_data'] = csv

        # If (bitmap) annotation exists
        if Annotation.objects.filter(spim_id=pk).exists():
            annotation = Annotation.objects.get(spim_id=pk)
            item['annot_pk'] = annotation.pk
            masks = IndividualMask.objects.filter(annotation_id=annotation)
            mask_list = list()
            for m in masks:
                mask_list.append({
                    'pk': m.pk,
                    'label': m.label,
                    'png_path': m.path_to_png
                })
            item['annot_pk'] = annotation.pk
            item['mask_list'] = mask_list

        return render(request, 'placenta/spim.html', {'item': item, 'pk': pk, 'annform': annform, 'maskform': maskform})
    
    elif request.method == 'POST': # DELETE spim

        SpectralImage.objects.filter(pk=pk).delete()

        return(redirect(spectral_images))

    return (redirect(home))


@api_view(['POST'])
def update_spim_metadata(request, pk):
    """
    /spim/<str:pk>/update/  POST    update spim metadata
    """

    data = request.data.dict()
    if request.method == 'POST' and data['method_'] == 'PUT':

        # TODO: validation

        SpectralImage.objects.filter(pk=pk).update(
            spim_cube_metadata=data['metadata'],
            patient_id=data['patient_id'],
            diagnosis=data['diagnosis'],
            image_identifier=data['image_identifier'],
            image_series_identifier=data['image_series_identifier'],
            hsi_camera_name=data['hsi_camera_name'],
            optic_imaging_system=data['optic_imaging_system'],
            hsi_adapter_name=data['hsi_adapter_name'],
            environment_description=data['environment_description'],
            reference=data['reference'],
            light_source_description=data['light_source_description'],
            hsi_software_name=data['hsi_software_name'],
            hsi_software_version=data['hsi_software_version'],
            acquisition_type=data['acquisition_type'],
            hsi_sequence_name=data['hsi_sequence_name'],
            hsi_sequence_version=data['hsi_sequence_version'],
            data_size=data['data_size'],
            frames=int(data['frames']),
            imaging_light_intensity=float(data['imaging_light_intensity']),
            imaging_magnification=float(data['imaging_magnification']),
            imaging_focal_length=float(data['imaging_focal_length']),
            notes=data['notes']
        )
        if data['timestamp']:
            SpectralImage.objects.filter(pk=pk).update(
                timestamp=data['timestamp']
            )

        return(redirect('./'))
    
    return(redirect(home))


@api_view(['POST'])
def upload_annotation(request, pk):
    """
    /spim/<str:pk>/uploadmask/  POST    create new annotation
    """

    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        for f in request.FILES.getlist('file'):

            name = str(f)

            spectral_image = SpectralImage.objects.get(pk=pk)
            # If annotation exists, delete it (one and only one relation)
            if Mask.objects.filter(spim_id=spectral_image).exists():
                Mask.objects.filter(spim_id=spectral_image).delete()

            if Annotation.objects.filter(spim_id=spectral_image).exists():
                Annotation.objects.filter(spim_id=spectral_image).delete()

            # Initialize Annotation instance for created spim
            Annotation.objects.create(
                label = name[:-4],
                spim_id = spectral_image
            )

            # from csv file
            if name[-4:] == '.csv':

                data = f.read().decode('utf-8')
                parsed = parse(data)

                # Old legacy mask object, to be deleted
                Mask.objects.create(
                    spim_id=spectral_image,
                    file_name=name[:-4],
                    csv_data=data,
                    set_name=name[0:5]
                )
                
                masks = create_masks(parsed,1024,1024) # TODO: remove hardcoded values, (how?)
                flattened = flatten_masks(masks)
                create_mask_instance(flattened, name, pk)

            # from tif file
            elif name[-4:] == '.tif' or name[-5:] == '.tiff':
                
                # Write file in memory to disk (is there way to read_mtiff() from memory?)
                with default_storage.open('temp/' + name, 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

                flattened = read_mtiff('placenta/files/temp/' + name)
                create_mask_instance(flattened, name, pk)   

        return(redirect('./'))

    return (redirect(home))


@api_view(['GET', 'POST'])
def delete_annotation(request, pk):
    """
    /spim/<str:pk>/deletemask/  POST    DELETE annotation
    """

    if request.method == 'POST': # (DELETE)

        Mask.objects.filter(pk=pk).delete()
        Annotation.objects.filter(pk=pk).delete()

        return(redirect('spim', pk=pk))

    return (redirect(home))

def metadata_page(request, pk):

    if request.method == 'GET':

        spim = SpectralImage.objects.get(pk=pk)
        
        with h5py.File(spim.path_to_hdf5, 'r') as f:
            numpy_spim = np.array(f['spim'])
            numpy_wavelengths = np.array(f['wavelengths'])
            numpy_rgb = np.array(f['rgb'])

        return render(request, 'placenta/metadata.html', {
            'pk': pk,
            'file_name': spim.label,
            'path_to_thumbnail': spim.path_to_thumbnail,
            'metadata': spim.spim_cube_metadata,
            'patient_id': spim.patient_id,
            'diagnosis': spim.diagnosis,
            'image_identifier': spim.image_identifier,
            'image_series_identifier': spim.image_series_identifier,
            'hsi_camera_name': spim.hsi_camera_name,
            'optic_imaging_system': spim.optic_imaging_system,
            'hsi_adapter_name': spim.hsi_adapter_name,
            'environment_description': spim.environment_description,
            'reference': spim.reference,
            'light_source_description': spim.light_source_description,
            'hsi_software_name': spim.hsi_software_name,
            'hsi_software_version': spim.hsi_software_version,
            'acquisition_type': spim.acquisition_type,
            'hsi_sequence_name': spim.hsi_sequence_name,
            'hsi_sequence_version': spim.hsi_sequence_version,
            'timestamp': spim.timestamp.isoformat(),
            'data_size': spim.data_size,
            'resolution': tuple(numpy_spim.shape),
            'bandpass_range': tuple((min(numpy_wavelengths), max(numpy_wavelengths))), # TODO
            'frames': spim.frames,
            'imaging_light_intensity': spim.imaging_light_intensity,
            'imaging_magnification': spim.imaging_magnification,
            'imaging_focal_length': spim.imaging_focal_length,
            'notes': spim.notes
        })
        
    return (redirect(home))

@api_view(['POST'])
def create_individual_mask(request, pk):
    """
    create individual mask (from png), not whole annotation
    """

    data = request.data.dict()
    # Get reference to annotation object
    annotation = Annotation.objects.get(pk=data['annot_pk'])

    form = UploadFileForm(request.POST, request.FILES)
    for f in request.FILES.getlist('file'):

        png_path = 'masks/' + str(uuid.uuid4()) + '.png'
        # Create png of the mask
        with default_storage.open(png_path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

        IndividualMask.objects.create(
            annotation_id = annotation,
            path_to_png = '/files/' + png_path,
            label = data['label']
        )
    
    return redirect('spim', pk=pk)


@api_view(['POST'])
def update_individual_mask(request, pk):
    """
    update individual mask (from png), not whole annotation
    """

    data = request.data.dict()
    # Get reference to annotation 
    annotation = Annotation.objects.get(pk=data['annot_pk'])

    # Update label only
    if not request.FILES.getlist('file'):
        mask = IndividualMask.objects.get(pk=data['mask_pk'])
        mask.label = data['mask_label']
        mask.save()
        return redirect('spim', pk=pk)

    form = UploadFileForm(request.POST, request.FILES)
    for f in request.FILES.getlist('file'):

        mask = IndividualMask.objects.get(pk=data['mask_pk'])

        # Create png of the mask
        png_path = 'masks/' + str(uuid.uuid4()) + '.png'
        with default_storage.open(png_path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        # Update path field
        
        mask.path_to_png = '/files/' + png_path
        mask.label = data['mask_label']
        mask.save()
    
    return redirect('spim', pk=pk)


@api_view(['POST'])
def delete_individual_mask(request, pk):
    """
    delete individual mask, not whole annotation
    """

    data = request.data.dict()
    IndividualMask.objects.filter(pk=data['mask_pk']).delete()

    return(redirect('spim', pk=pk))

