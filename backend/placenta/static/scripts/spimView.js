            //TO DO: Clean up and comment the code
            //TO DO: Change the order of the masks on sidelist
            const screenSize = {
                w: window.innerWidth,
                h: window.innerHeight,
                scale: 1,
            }
            const fixedWaveArray = [];
            let currentBand = null;
            let rgbImageRender = {
                exists: false,
                image: null,
            }
            waveArray.forEach((currentWavelength)=>{
                if(currentWavelength!==''){
                    fixedWaveArray.push(currentWavelength.trim());
                }
            });
            let currentState = 'rgb';

            //Main render function
            const render = function(){
                const canvasObj = document.getElementById("view-canvas");
                document.getElementById('bandSlider').max = waveArray.length-1;

                const savedRender = {
                    exists: false,
                    image: [],
                }


                console.info("Size of the current image: "+rgbImage.w+", "+rgbImage.h);
                
                const mainCanvas = {
                    c: canvasObj.getContext("2d"),
                    w: rgbImage.w,
                    h: rgbImage.h,
                    mouseX: 0,
                    mouseY: 0,
                }

                //Resizes the canvas object according to the screensize
                const screenSizeCheck = function(){
                    console.log("resize");
                    screenSize.w = window.innerWidth;
                    screenSize.h = window.innerHeight;
                    if(rgbImage.h > screenSize.h-canvasObj.getBoundingClientRect().y-15){
                        screenSize.scale = (window.innerHeight-canvasObj.getBoundingClientRect().y-15)/rgbImage.h;
                        canvasObj.style.width = Math.floor((screenSize.scale*(rgbImage.w)))+"px";
                        canvasObj.style.height = Math.floor(screenSize.scale*rgbImage.h)+"px";
                        const viewDiv = document.getElementById('viewDiv');
                        viewDiv.style.maxHeight = Math.floor((screenSize.scale*(rgbImage.h)))+"px";
                    }
                }
                window.addEventListener("resize",()=>{screenSizeCheck();});
                canvasObj.style.width = mainCanvas.w+"px";
                canvasObj.style.height = mainCanvas.h+"px";
                canvasObj.width = mainCanvas.w;
                canvasObj.height = mainCanvas.h;
                screenSizeCheck();

                //Adds the rgb image on the canvas
                const renderImage = function(){
                    if(rgbImageRender.exists){
                        mainCanvas.c.putImageData(rgbImageRender.image,0,0);
                    }else{
                        const newImage = mainCanvas.c.createImageData(rgbImage.w,rgbImage.h);
                        for(i=0;i<rgbImage.rgbData.length/3;i++){
                            newImage.data[(i*4)] = rgbImage.rgbData[(i*3)];
                            newImage.data[(i*4)+1] = rgbImage.rgbData[(i*3)+1];
                            newImage.data[(i*4)+2] = rgbImage.rgbData[(i*3)+2];
                            newImage.data[(i*4)+3] = 255;
                        }
                        rgbImageRender.image = newImage;
                        rgbImageRender.exists = true;
                        mainCanvas.c.putImageData(newImage,0,0);
                    }
                }

                //Adds the current band image to the canvas
                const renderBand = function(){
                    if(currentBand!=null&&currentBand.exists){
                        mainCanvas.c.putImageData(currentBand.image,0,0);
                    }else{
                        const givenWidth = currentBand.width;
                        const givenHeight = currentBand.height;
                        const band = currentBand.band;
                        const newImage = mainCanvas.c.createImageData(givenWidth,givenHeight);
                        bandData = band;
                        bandData.forEach((currentValue,currentIndex)=>{
                            newImage.data[currentIndex*4] = Math.round(currentValue*255);
                            newImage.data[currentIndex*4+1] = Math.round(currentValue*255);
                            newImage.data[currentIndex*4+2] = Math.round(currentValue*255);
                            newImage.data[currentIndex*4+3] = 255;
                        });
                        currentBand.image = newImage;
                        currentBand.exists = true;
                        mainCanvas.c.putImageData(newImage,0,0);
                    }
                }

                //Clears the canvas and sets the default settings
                const clearAll = function(){
                    const saveCurrentColor = mainCanvas.c.fillStyle;
                    mainCanvas.c.clearRect(0,0,mainCanvas.w, mainCanvas.h);
                    mainCanvas.c.fillStyle = "#FFFFFF";
                    mainCanvas.c.fillStyle = saveCurrentColor;
                    if(currentState=='rgb'){
                        renderImage();
                    }else{
                        renderBand();
                    }
                }

                //Returns a color for the index
                const getColorByIndex = function(givenIndex){
                    //console.log(givenIndex);
                    switch(parseInt(givenIndex)){
                        case 0:return '#FFFF00';
                        case 1:return '#FF00FF';
                        case 2:return '#00FFFF';
                        case 3:return '#FF0000';
                        case 4:return '#0000FF';
                        case 5:return '#00FF00';
                        default:return '#FFFFFF';
                    }
                }
                
                //Adds all the masks on the canvas
                const drawMasks = function(){
                    allMasks.forEach((currentMask)=>{
                        if(currentMask.on){
                            mainCanvas.c.drawImage(document.getElementById('mask_'+currentMask.pk),0,0);
                        }
                    });

                }

                //Toggles between showing and not showing the given mask by py
                const toggleMask = function(givenPk){
                    for(let masksIndex = 0;masksIndex<allMasks.length;masksIndex++){
                        if(allMasks[masksIndex].pk==givenPk){
                            console.log(givenPk);
                            if(allMasks[masksIndex].on){
                                document.getElementById('mask_'+givenPk).style.borderColor = '#000000';
                                document.getElementById('mask_row_'+givenPk).style.borderColor = '#555555';
                                allMasks[masksIndex].on = false;
                            }else{
                                document.getElementById('mask_'+givenPk).style.borderColor = '#FFFFFF';
                                document.getElementById('mask_row_'+givenPk).style.borderColor = getColorByIndex(allMasks[masksIndex].pk-allMasks[0].pk);
                                allMasks[masksIndex].on = true;
                            }
                            update();
                            drawMasks();
                            break;
                        }
                    }
                }

                //Colors the mask pngs and adds the toggles to the mask thumbnails
                const addTogglesAndColors = function(){
                    const offCanvas = new OffscreenCanvas(mainCanvas.w,mainCanvas.h);
                    const offCanvasContext = offCanvas.getContext('2d');
                    allMasks.forEach((currentMask)=>{
                        const currentThumbnail = document.getElementById('mask_'+currentMask.pk);
                        currentThumbnail.addEventListener('click',()=>{
                            toggleMask(currentMask.pk);
                        });
                        offCanvasContext.clearRect(0,0,mainCanvas.w,mainCanvas.h);
                        const currentMaskColor = getColorByIndex(currentMask.pk-allMasks[0].pk);
                        offCanvasContext.fillStyle = currentMaskColor;
                        document.getElementById('mask_row_'+currentMask.pk).style.borderColor = currentMaskColor;
                        document.getElementById('mask_'+currentMask.pk).style.borderColor = '#FFFFFF';
                        offCanvasContext.drawImage(document.getElementById('mask_'+currentMask.pk),0,0);
                        offCanvasContext.globalCompositeOperation = 'source-in';
                        offCanvasContext.fillRect(0,0,mainCanvas.w,mainCanvas.h);
                        offCanvasContext.globalCompositeOperation = 'source-over';
                        mainCanvas.c.putImageData(offCanvasContext.getImageData(0,0,mainCanvas.w,mainCanvas.h),0,0);
                        document.getElementById('mask_'+currentMask.pk).src = canvasObj.toDataURL();
                    });
                }
                addTogglesAndColors();

                //Redraws everything on main canvas
                const update = function(){
                    if(!savedRender.exists){
                        clearAll();
                        drawMasks(givenMasksCSV);
                    }else{
                        mainCanvas.c.putImageData(savedRender.image,0,0);
                    }
                }
                update();

                const fetchedBands = [];
                const fetchedBandsLimit = 10;
                //Adds the fetched band to the list of fetched as an object
                const processBandData = function(givenData,givenIndex){
                    currentJSON = JSON.parse(givenData);
                    console.info('Band: '+currentJSON.width+', '+currentJSON.height);
                    currentBand = {
                        band: currentJSON.band.replace(/\[/g,'').replace(/\]/g,'').split(","),
                        width: currentJSON.width,
                        height: currentJSON.height,
                        exists: false,
                        image: null,
                    }
                    fetchedBands.push({fetchedIndex:givenIndex,fetchedBand:currentBand});
                    if(fetchedBands.length>fetchedBandsLimit){
                        fetchedBands.shift();
                    }
                    savedRender.exists=false;
                    document.getElementById('stateButton').style.visibility = 'visible';
                    document.getElementById('wavelengthName').innerHTML = 'Current Wavelength: '+fixedWaveArray[parseInt(document.getElementById('bandSlider').value)];
                    update();
                }

                //Fetches the currently selected band
                const getBands = function(){
                    if(currentState=='rgb'){
                        changeState();
                    }
                    let currentData = '';
                    const currentIndex = document.getElementById('bandSlider').value;
                    document.getElementById('wavelengthName').innerHTML = 'Fetching';
                    fetch(window.location+'bands/'+document.getElementById('bandSlider').value.toString()).then((res)=>res.text()).then((data)=>processBandData(data,currentIndex));
                }

                //Checks if the currently selected band has already been fetched
                const checkBands = function(givenIndex){
                    if(fetchedBands.length>0){
                        for(let currentFetched=0;currentFetched<fetchedBands.length;currentFetched++){
                            if(givenIndex==fetchedBands[currentFetched].fetchedIndex){
                                currentBand = fetchedBands[currentFetched].fetchedBand;
                                savedRender.exists=false;
                                update();
                                return true;
                            }
                        }
                    }
                    return false;
                }
                document.getElementById('bandSlider').addEventListener('mouseup',()=>{if(!checkBands(document.getElementById('bandSlider').value)){getBands()}});
                document.getElementById('bandSlider').addEventListener('input',()=>checkBands(document.getElementById('bandSlider').value));
                document.getElementById('stateButton').addEventListener('click',()=>{changeState();savedRender.exists=false;update();});
                updateWavelengthText();
                getBands();
            }

            //Updates the text for current selected band
            const updateWavelengthText = function(){
                const check = fixedWaveArray[parseInt(document.getElementById('bandSlider').value)];
                if(check!==undefined){
                    document.getElementById('wavelengthName').innerHTML = 'Current Wavelength: '+fixedWaveArray[parseInt(document.getElementById('bandSlider').value)];
                }
            }

            //Changes between showing the RGB and current band
            const changeState = function(){
                if(currentState=='rgb'){
                    currentState = 'bands';
                    document.getElementById('stateButton').innerHTML = 'RGB';
                }else{
                    currentState = 'rgb';
                    document.getElementById('stateButton').innerHTML = 'Bands';
                }
            }

            //Waits for the DOM to finish loading
            window.addEventListener("DOMContentLoaded",()=>{render();});