from django.apps import AppConfig


class PlacentaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'placenta'
