import numpy as np

def apply_mask_to_img(img: np.array,binary_mask: np.array):
    # Img can be any channel size
    # Mask needs to be binary (True of False)
    size_original = img.shape
    # Convert mask from Binary to 0 and 1
    int_mask = np.zeros_like(binary_mask, dtype=img.dtype)
    int_mask[binary_mask == True] = 1
    #int_mask[binary_mask == False] = np.nan

    # Copy int_mask into a 3rd Dimension for the number of channels
    num_channels = img.shape[2]
    int_mask = np.repeat(int_mask[:, :, np.newaxis], num_channels, axis=2)

    # Instead of multiply a mask in 3D with the image  we'll flatten and come back
    #int_mask = int_mask.flatten()
    #img = img.flatten()
    img_masked = np.multiply(img,int_mask)

    ################################################
    # Estimate Mean only for True on binary Mask
    ################################################
    spectral_image_flatten = np.reshape(img_masked,(np.prod(img_masked.shape[0:2]),img_masked.shape[2]))
    binary_mask_flatten = np.reshape(binary_mask,(np.prod(img_masked.shape[0:2]),1))

    image_flattened_filtered = spectral_image_flatten[binary_mask_flatten[:,0],:]

    mean_per_band = np.mean(image_flattened_filtered, axis = 0)

    #img_masked = np.reshape(img_masked, size_original)
    return img_masked, mean_per_band, image_flattened_filtered