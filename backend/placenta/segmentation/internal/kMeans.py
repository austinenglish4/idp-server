from sklearn import cluster
import numpy as np
import cv2

def start(n_channel_img, n_clusters, plot = False, title_sufix = "", originalMask=[],resize_output=True, original_shape=()):
    ########################
    # Scikit K-Means
    ########################
    stacked_img_reshaped = n_channel_img.reshape((-1, n_channel_img.shape[2]))

    k_means = cluster.KMeans(n_clusters)
    k_means.fit(stacked_img_reshaped)
    X_cluster = k_means.labels_
    if len(n_channel_img.shape) == 3:
        X_cluster = X_cluster.reshape(n_channel_img[:, :, 0].shape)
    else:
        X_cluster = X_cluster.reshape(n_channel_img.shape)

    final_final_image = X_cluster.copy()
    for ele_i, ele in enumerate(np.unique(X_cluster)):
        final_final_image[final_final_image == ele] = ele_i

    if resize_output:
        final_final_image = cv2.resize(final_final_image, original_shape, 0, 0, interpolation=cv2.INTER_NEAREST)

    return final_final_image