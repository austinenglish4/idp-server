import numpy as np
from skimage.measure.entropy import shannon_entropy
###########################################################
# Select Particular Bands That have the most information
###########################################################

# Based on "Hyperspectral Band Selection via Optimal Combination Strategy" from Shuying Li et al.


def start(specim, m_subspaces = 7):
    if specim.shape[2] != m_subspaces:
        print("===============  Selecting Optimal Bands ===============")
        # Number of bands
        L = specim.shape[2]
        #########################
        # Coarse Grouping
        #########################
        # List to store the subspaces indices
        coarse_subspace_list = [ [0]*2 for i in range(m_subspaces)]
        # Get if the number of bands can fit in the m_subspaces selected
        combination = L % m_subspaces
        # Subspace number (index 1)
        g = 1
        if combination == 0:
            for sl in coarse_subspace_list:
                idx_start = ( ( L * (g-1) ) / m_subspaces ) +1
                idx_finish = L * g / m_subspaces
                sl[0] = idx_start
                sl[1] = idx_finish
                g+=1 # Increase subspace number
        else:
            for sl in coarse_subspace_list:
                idx_start = 0
                idx_finish = 0
                if g != m_subspaces:
                    idx_start = np.floor( ( ( L * (g-1) ) / m_subspaces ) +1 )
                    idx_finish = np.floor( L * g / m_subspaces )
                else:
                    idx_start = np.floor( ( ( L * (g-1) ) / m_subspaces ) + 1)  # Paper says to use idx_start = L - ( L % m_subspaces ) + 1    but it's not consistent
                    idx_finish = L

                sl[0] = idx_start
                sl[1] = idx_finish
                g+=1 # Increase subspace number



        # Adjust to index 0 by removing 1 of every element
        coarse_subspace_list = np.array(coarse_subspace_list, dtype=np.uint8)
        coarse_subspace_list -= 1

        #print("Coarse: ",coarse_subspace_list)

        #########################
        # Fine Grouping
        #########################

        # Measure Similarity of Bands using "Local Density" metric

        # Create Euclidian Distance Matrix Between Bands
        # This is the similarity Matrix
        s_matrix = np.empty((L,L), dtype=float)
        for i in range(L): # Rows
            for j in range(L): # cols
                current_band = specim[:,:,i].flatten()
                other_band = specim[:,:,j].flatten()
                s_matrix[i,j] = np.sqrt( np.sum( np.square(current_band - other_band) ) )

        # Sort Similarity Matrix in ascending order by ROW
        # The paper says column but its a symmetric matrix, it doesnt matter as long
        # as you do the summation starting from m_subspaces
        A_matrix = np.sort(s_matrix, axis=1) #Ascending order Matrix

        # The threshold per row (per band)
        sigma = np.empty((L,1),dtype=float)

        for ii in range(L): # Row
            sigma[ii,0] = np.sum(A_matrix[ii,m_subspaces+1 :]) / (L - m_subspaces)

        # Local Density of each band
        mu = np.empty((L,1),dtype=float)
        for sigma_i, sigma_vl in enumerate(sigma):
            tmp_row = A_matrix[sigma_i,:]
            test_bool = tmp_row < sigma_vl
            ni = np.sum(test_bool)

            # Obtain sigma for bands whose distance from the i-th band is less than the threshold sigma_vl
            mu[sigma_i,0] = ni / L


        for g_idx in np.arange(0,m_subspaces-1):
            Zg = coarse_subspace_list[g_idx][0]
            Zg_1 = coarse_subspace_list[g_idx+1][0]

            alpha = ( Zg + Zg_1 ) / 2

            if g_idx != (m_subspaces-2):
                Zg_2 = coarse_subspace_list[g_idx+2][0]
                beta = ( Zg_1 + Zg_2 ) / 2
            else:
                beta = ( Zg_1 + L) / 2

            idx_start = np.round(alpha).astype(int)
            idx_finish = np.round(beta).astype(int)
            density_local_tmp = mu[idx_start:idx_finish+1,0]
            min_density_idx = np.argmin(density_local_tmp)

            idx_to_update = min_density_idx + idx_start

            # Update Upper Bound of Current g subspace and next one
            coarse_subspace_list[g_idx][1] = idx_to_update
            coarse_subspace_list[g_idx+1][0] = idx_to_update + 1


        #########################
        # Band Entropy
        #########################

        band_entropy = np.empty((L,1),dtype=float)

        for ll in range(L):
            band_entropy[ll,0] = shannon_entropy(specim[:,:,ll])

        #########################
        # Normalization of Entropy and Similarity Matrix
        #########################
        band_entropy = (band_entropy - np.min(band_entropy)) / (np.max(band_entropy) - np.min(band_entropy))


        # TODO: Check if its the row/col max and min or should it be one for the entire matrix
        s_matrix = (s_matrix - np.min(s_matrix, axis=0)) / (np.max(s_matrix, axis=0) - np.min(s_matrix,axis=0))

        #########################
        # Get evaluation Function
        #########################

        representative_bands = np.empty((m_subspaces,2),dtype=np.uint8)
        # For bands in the same subspace
        for ele_idx,ele in enumerate(coarse_subspace_list):
            evaluation_r = np.zeros_like(s_matrix)
            for i in np.arange(ele[0],ele[1]+1):
                for j in range(ele[0],ele[1]+1):
                    if i != j:
                        evaluation_r[i,j] = ( band_entropy[i] * band_entropy[j]) / s_matrix[i,j]

            # Because it's a symmetric matrix we only take the lowe part
            #evaluation_r = np.tril(evaluation_r)
            # Select the 2 bands with the highest value in subspace
            k1_idxs = np.unravel_index(evaluation_r.argmax(), evaluation_r.shape)
            #print("K1 Coord: ",k1_idxs)
            k1_tmp = k1_idxs[1] # Row is the candidate
            evaluation_r[k1_idxs] = 0 # Make it 0 to find the second max one
            while(True):
                k2_idxs = np.unravel_index(evaluation_r.argmax(), evaluation_r.shape)
                k2_tmp = k2_idxs[1] # Row is the candidate
                #print("K2 Coord: ", k2_idxs)
                #if (k2_tmp != k1_tmp) and (k2_tmp not in representative_bands.flatten()):
                if (k2_tmp != k1_tmp):
                    break;
                elif not evaluation_r.any():
                    break;
                else:
                    evaluation_r[k2_idxs] = 0 # Make it 0 to find the second max one



            representative_bands[ele_idx,0] = k1_tmp
            representative_bands[ele_idx,1] = k2_tmp

        #########################
        # Optimal Combination Strategy (OCS) Algorithm
        #########################
        def eval_phi(combinationPhi):
            #print(combinationPhi)
            m_temp = len(combinationPhi)
            element1 = np.sum([band_entropy[x] for x in combinationPhi]) / m_temp
            cont = 0
            element2 = 0
            for _ in range(m_temp-1):
                element2 += s_matrix[combinationPhi[cont],combinationPhi[cont+1]]
                cont += 1
            element2 = element2 / (m_temp - 1)

            return element1*element2

        R_optimal = []
        for n in range(m_subspaces):
            Rn = 0
            # For First Subspace
            if n == 0:
                H_K1_0 = band_entropy[representative_bands[n][0]] # Entropy of represetnative 1, subspace 0
                H_K2_0 = band_entropy[representative_bands[n][1]] # Entropy of represetnative 2, subspace 0
                if H_K1_0 > H_K2_0:
                    R1 = representative_bands[n][0]
                else:
                    R1 = representative_bands[n][1]
            # For the rest of Subspaces
            else:
                # Initialize with R1 of n=0
                R_candidate1 = [R1]
                R_candidate2 = [R1]
                # Fill with Rest of Representative Candidates
                for r in np.arange(1,n+1):
                    R_candidate1.append(representative_bands[r][0])
                    R_candidate2.append(representative_bands[r][1])

                phiEval1 = eval_phi(R_candidate1)
                phiEval2 = eval_phi(R_candidate2)

                if phiEval1 > phiEval2:
                    Rn = representative_bands[n][0]
                else:
                    Rn = representative_bands[n][1]

            # Optimal Bands
            R_optimal.append(Rn)

        #R_optimal = R_optimal[1:]
        R_optimal = np.unique(R_optimal)
        optimal_spim = specim[:,:,R_optimal]
        print(R_optimal)
    else:
        R_optimal = range(specim.shape[2]+1)
        optimal_spim = specim
    return R_optimal, optimal_spim