###################################
# Import Python Libraries
###################################
from skimage.transform import resize
import numpy as np

###################################
# Import Custom Libraries
###################################
from . import kMeans
from . import hyperspectral_band_selection
from . import advanced_DL
from . import spectral_kmeans
from . import resnet
from . import simple_DL

###################################################
# Get Optimal Bands and/or Resize Spectral Cube
###################################################
def transform_hypercube(spim, optimal_hypercube=True, crop_hypercube=False):
    n_channel_img = []
    if optimal_hypercube:
        _, n_channel_img = hyperspectral_band_selection.start(spim, m_subspaces=11)
    else:
        n_channel_img = spim

    if crop_hypercube:
        scale_f = 0.3
        n_channel_img = resize(n_channel_img,
                               (np.ceil(spim.shape[0] * scale_f), np.ceil(spim.shape[1] * scale_f), spim.shape[2]))

    return n_channel_img


def get_empty_mask(spim):
    return np.zeros_like(spim)
###################################
# Run Segmentation Method
###################################
# -----------------------------------------------------------
# ----------------- NormalK-Means ---------------------------
# -----------------------------------------------------------
def run_normalkmeans(spim):
    n_clusters = 8  # Default is 8
    n_channel_image = transform_hypercube(spim, optimal_hypercube=True, crop_hypercube=True)

    return kMeans.start(n_channel_img=n_channel_image,n_clusters=n_clusters, original_shape=spim.shape[0:2])

# -----------------------------------------------------------
# ----------------- Advanced DL ---------------------------
# -----------------------------------------------------------
def run_combination_advanced_dl(spim):
    combination_Advanced_DL = 0  # Default is 0 ( From 0 to 9)
    n_clusters = 8  # Default is 8
    n_channel_image = transform_hypercube(spim, optimal_hypercube=False, crop_hypercube=False)

    return advanced_DL.start(n_channel_img=n_channel_image, combination=combination_Advanced_DL, minLabels = n_clusters, original_shape=spim.shape[0:2])

# -----------------------------------------------------------
# ----------------- Simple DL ---------------------------
# -----------------------------------------------------------
def run_simple_dl(spim):    
    n_clusters = 8  # Default is 8
    pixel_mini_cluster = 4  # Default is 4
    n_channel_image = transform_hypercube(spim, optimal_hypercube=True, crop_hypercube=False)

    return simple_DL.start(n_channel_img=n_channel_image, original_shape=spim.shape[0:2], pixel_mini_cluster=pixel_mini_cluster, minLabels=n_clusters)


# -----------------------------------------------------------
# ----------------- ResNet ---------------------------
# -----------------------------------------------------------
def run_resnet(spim):
    return resnet.start(spim)

# -----------------------------------------------------------
# ----------------- Spectral K-Means  ---------------------------
# -----------------------------------------------------------
def run_spectral_kmeans(spim):
    n_clusters = 10  # Default is 10
    n_channel_image = transform_hypercube(spim, optimal_hypercube=True, crop_hypercube=True)

    return spectral_kmeans.start(n_channel_img=n_channel_image, n_clusters=n_clusters, original_shape=spim.shape[0:2])
