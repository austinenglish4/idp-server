from itertools import cycle
import matplotlib.cm as cm
import numpy as np
import h5py
from skimage import morphology as morf
from natsort import natsorted # to sort files
import os
from os import listdir
from os.path import isfile, join

def segments_color_map(n_clusters, color_style = 2):
    ####################################
    # Define Custom Color Map
    ####################################

    # Array of Colors
    if color_style == 1:
        colors = cycle(cm.tab10.colors)
    elif color_style == 2:
        colors = cycle(cm.tab20.colors)
    color_legends = []
    # Create Colormap
    viridis = cm.get_cmap('viridis', 256) # viridis is just for the format
    newcolors = viridis(np.arange(0, n_clusters))

    for i in range(n_clusters):
        color = next(colors)
        color_tmp = np.concatenate((color,[1]))

        # create a patch (proxy artist) for every color
        color_legends.append(mpatches.Patch(edgecolor=color_tmp,facecolor=color_tmp, label="Tissue {l}".format(l=i) ))

        # Append Color to new CMAP
        newcolors[i, :] = color_tmp
    segmentCMAP = ListedColormap(newcolors)

    return segmentCMAP, color_legends



def save_mask_hdf5(mask, file_name):
    hf = h5py.File(file_name+'.h5', 'w')
    for i in np.unique(mask):
        bin_mask = (mask == i)
        hf.create_dataset('dataset_'+str(i), data=bin_mask)
    hf.close()

def show_masks(mask_file, plot=False):
    shape_mask = mask_file[list(mask_file.keys())[0]].shape
    complete_mask = np.ones(shape_mask,dtype=int)*len(mask_file.keys())

    # Convert Mask to Indices
    for ik,ma in enumerate(mask_file.keys()):

        spim_mask = mask_file[ma]
        complete_mask[spim_mask] = ik

    new_labels_show_mask = len(np.unique(complete_mask))

    return complete_mask

def add_pixel_location_as_feature(n_channel_img ):
    # Reshape image for method
    if len(n_channel_img.shape) == 3:
        stacked_img_reshaped = n_channel_img.reshape((-1, n_channel_img.shape[2]))
    else:
        stacked_img_reshaped = n_channel_img.reshape((-1, 1))

    # Create Coordinate Meshgrid
    rows_i, cols_i = np.meshgrid(range(n_channel_img.shape[0]), range(n_channel_img.shape[1]), indexing='ij')
    rows_i = rows_i.reshape(-1, 1).astype(float)
    rows_i = rows_i / np.max(rows_i)
    cols_i = cols_i.reshape(-1, 1).astype(float)
    cols_i = cols_i / np.max(cols_i)
    pixel_location_feature = np.concatenate((rows_i, cols_i), axis=1)
    # Normalize

    stacked_img_reshaped = np.hstack((pixel_location_feature, stacked_img_reshaped))

    return stacked_img_reshaped

def improve_mask(mask_tmp, plot=False):
    # Merging First two masks
    final_image = mask_tmp.copy()

    masks = []

    # Order Masks with less pixels to bigger
    masks_idx_high_to_low_density = np.empty((len(np.unique(mask_tmp)),2),dtype=int)

    # Get Masks and Number of Pixels
    for i in np.sort(np.unique(mask_tmp)):
        old_mask = (mask_tmp == i)
        num_pixels = np.sum(old_mask)
        masks_idx_high_to_low_density[i,0] = i
        masks_idx_high_to_low_density[i,1] = num_pixels

        #binary_filled = morf.binary_closing(morf.remove_small_holes(old_mask,295))
        #binary_filled = morf.area_closing(old_mask,area_threshold=180095)
        binary_filled = morf.remove_small_holes(old_mask,134375)
        #binary_filled = morf.convex_hull_image(old_mask)

        masks.append(binary_filled)

    # Sort pixels by number of pixels
    masks_idx_high_to_low_density = masks_idx_high_to_low_density[masks_idx_high_to_low_density[:, 1].argsort()][::-1]

    for idx in masks_idx_high_to_low_density[:,0]:
        current_new_mask = masks[idx]
        final_image[current_new_mask] = idx #keep same index as before



    new_labels = len(np.unique(mask_tmp))
    segmentCMAP, color_legends = segments_color_map(n_clusters=new_labels)


    new_labels2 = len(np.unique(final_image))

    return final_image

def get_folder_files(rel_path_hsi,rel_path_mask ):
    # Read HSI with Field Flattened Correction
    cwd = os.getcwd()
    abs_file_path_hsi = os.path.join(cwd, rel_path_hsi)
    onlyfiles_HSI = natsorted([f for f in listdir(abs_file_path_hsi) if isfile(join(abs_file_path_hsi, f))])

    abs_file_path_mask = os.path.join(cwd, rel_path_mask)
    onlyfiles_mask = natsorted([f for f in listdir(abs_file_path_mask) if isfile(join(abs_file_path_mask, f))])

    return onlyfiles_HSI, onlyfiles_mask