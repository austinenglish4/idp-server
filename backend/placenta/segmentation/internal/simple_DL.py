import torch
import imageio
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import skimage.segmentation as seg

import numpy as np
import cv2
from .utilities import segments_color_map
import math
def cyclical_lr(stepsize, min_lr=3e-4, max_lr=3e-3):
    # Scaler: we can adapt this if we do not want the triangular CLR
    scaler = lambda x: 1.

    # Lambda function to calculate the LR
    lr_lambda = lambda it: min_lr + (max_lr - min_lr) * relative(it, stepsize)

    # Additional function to see where on the cycle we are
    def relative(it, stepsize):
        cycle = math.floor(1 + it / (2 * stepsize))
        x = abs(it / stepsize - 2 * cycle + 1)
        return max(0, (1 - x)) * scaler(cycle)

    return lr_lambda

def start(n_channel_img,originalMask=[],title_sufix =' ',plot=False,cyclic=True, resize_output=True, original_shape=(), pixel_mini_cluster = 8, minLabels=8):
    use_cuda = torch.cuda.is_available()

    torch.manual_seed(0)
    torch.use_deterministic_algorithms(True)

    print('PyTorch Simple Unsupervised Segmentation')
    nChannel = 30  # Number of Channels
    maxIter = 1500  # number of maximum iterations
    #minLabels = 8  # minimum number of labels
    lr = 0.45  # Learning Rate
    nConv = 2  # number of convolutional layers
    num_superpixels = 10000  # number of superpixels
    compactness = pixel_mini_cluster  # compactness of superpixels
    visualize = 0  # visualization flag

    # Release memory
    torch.cuda.empty_cache()

    # CNN model
    class MyNet(nn.Module):
        def __init__(self, input_dim):
            super(MyNet, self).__init__()
            self.conv1 = nn.Conv2d(input_dim, nChannel, kernel_size=3, stride=1, padding=1)
            self.bn1 = nn.BatchNorm2d(nChannel)
            self.conv2 = nn.ModuleList()
            self.bn2 = nn.ModuleList()
            for i in range(nConv - 1):
                self.conv2.append(nn.Conv2d(nChannel, nChannel, kernel_size=3, stride=1, padding=1))
                self.bn2.append(nn.BatchNorm2d(nChannel))
            self.conv3 = nn.Conv2d(nChannel, nChannel, kernel_size=1, stride=1, padding=0)
            self.bn3 = nn.BatchNorm2d(nChannel)

        def forward(self, x):
            x = self.conv1(x)
            x = F.selu(x)
            x = self.bn1(x)
            for i in range(nConv - 1):
                x = self.conv2[i](x)
                x = F.selu(x)
                x = self.bn2[i](x)
            x = self.conv3(x)
            x = self.bn3(x)
            return x

    # load image
    im = n_channel_img
    # data = torch.from_numpy( np.array([im.transpose( (2, 0, 1) ).astype('float32')/255.]) )
    data = torch.from_numpy(np.array([im.transpose((2, 0, 1)).astype('float32')]))
    if use_cuda:
        data = data.cuda()
    data = Variable(data)

    ################################################
    # Initial Sub-Segmentation with: SLIC /
    ################################################
    sub_segm_method = 1
    if sub_segm_method == 1:
        labels = seg.slic(im, compactness=compactness, n_segments=num_superpixels)
        # labels = seg.slic(im, n_segments=num_superpixels)
    elif sub_segm_method == 2:
        labels = seg.felzenszwalb(im, scale=10, sigma=0.5, min_size=15)
    elif sub_segm_method == 3:
        labels = seg.quickshift(im, kernel_size=3, max_dist=6, ratio=0.5)

    labels = labels.reshape(im.shape[0] * im.shape[1])
    u_labels = np.unique(labels)
    l_inds = []
    for i in range(len(u_labels)):
        l_inds.append(np.where(labels == u_labels[i])[0])

    # train
    model = MyNet(data.size(1))
    if use_cuda:
        model.cuda()
    model.train()
    loss_fn = torch.nn.CrossEntropyLoss()

    if (cyclic):
        optimizer = optim.SGD(model.parameters(), lr=1., momentum=0.9)
        step_size = 32
        # step_size = 0.05
        clr = cyclical_lr(step_size, min_lr=0.01, max_lr=0.75)
        # clr = cyclical_lr(step_size, min_lr=0.00001, max_lr=0.01)
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, [clr])
    else:
        optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)



    label_colours = np.random.randint(255, size=(100, 3))
    for batch_idx in range(maxIter):
        # forwarding
        optimizer.zero_grad()
        output = model(data)[0]
        output = output.permute(1, 2, 0).contiguous().view(-1, nChannel)
        ignore, target = torch.max(output, 1)
        im_target = target.data.cpu().numpy()
        nLabels = len(np.unique(im_target))

        # superpixel refinement
        # TODO: use Torch Variable instead of numpy for faster calculation
        for i in range(len(l_inds)):
            labels_per_sp = im_target[l_inds[i]]
            u_labels_per_sp = np.unique(labels_per_sp)
            hist = np.zeros(len(u_labels_per_sp))
            for j in range(len(hist)):
                hist[j] = len(np.where(labels_per_sp == u_labels_per_sp[j])[0])
            im_target[l_inds[i]] = u_labels_per_sp[np.argmax(hist)]
        target = torch.from_numpy(im_target)
        if use_cuda:
            target = target.cuda()
        target = Variable(target)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        if (cyclic):
            scheduler.step()  # > Where the magic happens

        # print (batch_idx, '/', maxIter, ':', nLabels, loss.data[0])
        print(batch_idx, '/', maxIter, '|', ' label num :', nLabels, ' | loss :', loss.item(), ' | LR :',
              optimizer.param_groups[0]['lr'])

        if nLabels <= minLabels:
            print("nLabels", nLabels, "reached minLabels", minLabels, ".")
            break

    # save output image
    if not visualize:
        output = model(data)[0]
        output = output.permute(1, 2, 0).contiguous().view(-1, nChannel)
        ignore, target = torch.max(output, 1)
        im_target = target.data.cpu().numpy()
        # im_target_rgb = np.array([label_colours[ c % 100 ] for c in im_target])
        # im_target_rgb = im_target_rgb.reshape((im.shape[0],im.shape[1],3)).astype( np.uint8 )

        les_unique_labels = np.unique(im_target)
        im_target_rgb = im_target.copy()
        for idx, ele in enumerate(les_unique_labels):
            # Substitute values for idx from 0 to n_labels
            im_target_rgb[im_target_rgb == ele] = idx

        im_target_rgb = im_target_rgb.reshape((im.shape[0], im.shape[1], 1)).astype(np.uint8)

        if resize_output:
            im_target_rgb = cv2.resize(im_target_rgb, original_shape, 0, 0, interpolation=cv2.INTER_NEAREST)

    final_final_image = im_target_rgb.copy()
    for ele_i, ele in enumerate(np.unique(im_target_rgb)):
        final_final_image[final_final_image == ele] = ele_i

    return final_final_image