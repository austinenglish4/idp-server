import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

import math
import numpy as np
import cv2

# Custom Functio
from .utilities import segments_color_map


def cyclical_lr(stepsize, min_lr=3e-4, max_lr=3e-3):
    # Scaler: we can adapt this if we do not want the triangular CLR
    scaler = lambda x: 1.

    # Lambda function to calculate the LR
    lr_lambda = lambda it: min_lr + (max_lr - min_lr) * relative(it, stepsize)

    # Additional function to see where on the cycle we are
    def relative(it, stepsize):
        cycle = math.floor(1 + it / (2 * stepsize))
        x = abs(it / stepsize - 2 * cycle + 1)
        return max(0, (1 - x)) * scaler(cycle)

    return lr_lambda


def start(n_channel_img, combination=0, plot=False, originalMask=[], title_sufix=' ',cyclic=True, lr=0.3, minLabels=8, resize_output=True, original_shape=()):
    list_final = [[3, 0.6142, 0.2285], [3, 0.2285, 0.1], [2, 0.6142, 0.2285],  [2, 0.2285, 0.2285], [3, 0.3571, 0.1],
                  [3, 0.3571, 0.2285], [3, 0.4857, 0.2285],
                  [2, 0.6142, 0.1], [3, 0.8714, 0.4857], [3, 1.0, 0.6142]]

    ele = list_final[combination]

    ########################################################
    # Select Parameters based on Combination Number
    ########################################################
    nConv = ele[0]  # Standard 2 with lr = 0.3
    stepsize_sim = ele[1]
    stepsize_con = ele[2]
    nChannel = 30  # 40 for all spectra
    # Release memory
    torch.cuda.empty_cache()

    torch.manual_seed(0)
    torch.use_deterministic_algorithms(True)

    use_cuda = torch.cuda.is_available()

    print('PyTorch Advanced Unsupervised Segmentation')

    scribble = False  # Use scribbles
    #### with SELU
    # nChannel = 30 # number of channels
    # lr = 0.3 # learning rate
    # nConv = 4 # umber of convolutional layers

    #### with RELU
    # nChannel = 32  # number of channels
    # lr = 0.3  # learning rate
    # nConv = 2  # umber of convolutional layers

    maxIter = 500  # number of maximum iterations
    minLabels = 1  # minimum number of labels

    visualize = 0  # visualization flag
    # input = optimal_spim
    # stepsize_sim = 1  # step size for similarity loss
    # stepsize_con = 1  # step size for continuity loss
    stepsize_scr = 0.5  # step size for scribble loss

    # CNN model
    class MyNet(nn.Module):
        def __init__(self, input_dim):
            super(MyNet, self).__init__()
            self.conv1 = nn.Conv2d(input_dim, nChannel, kernel_size=3, stride=1, padding=1)
            self.bn1 = nn.BatchNorm2d(nChannel)
            self.conv2 = nn.ModuleList()
            self.bn2 = nn.ModuleList()
            for i in range(nConv - 1):
                self.conv2.append(nn.Conv2d(nChannel, nChannel, kernel_size=3, stride=1, padding=1))
                self.bn2.append(nn.BatchNorm2d(nChannel))
            self.conv3 = nn.Conv2d(nChannel, nChannel, kernel_size=1, stride=1, padding=0)
            self.bn3 = nn.BatchNorm2d(nChannel)

        def forward(self, x):
            x = self.conv1(x)
            x = F.relu(x)
            x = self.bn1(x)
            for i in range(nConv - 1):
                x = self.conv2[i](x)
                x = F.relu(x)
                x = self.bn2[i](x)
            x = self.conv3(x)
            x = self.bn3(x)
            return x

    # load image

    im = n_channel_img
    data = torch.from_numpy(np.array([im.transpose((2, 0, 1)).astype('float32')]))
    if use_cuda:
        data = data.cuda()
    data = Variable(data)

    # load scribble
    if scribble:
        mask = cv2.imread(input.replace('.' + input.split('.')[-1], '_scribble.png'), -1)
        mask = mask.reshape(-1)
        mask_inds = np.unique(mask)
        mask_inds = np.delete(mask_inds, np.argwhere(mask_inds == 255))
        inds_sim = torch.from_numpy(np.where(mask == 255)[0])
        inds_scr = torch.from_numpy(np.where(mask != 255)[0])
        target_scr = torch.from_numpy(mask.astype(np.int))
        if use_cuda:
            inds_sim = inds_sim.cuda()
            inds_scr = inds_scr.cuda()
            target_scr = target_scr.cuda()
        target_scr = Variable(target_scr)
        # set minLabels
        minLabels = len(mask_inds)

    # train
    model = MyNet(data.size(1))
    if use_cuda:
        model.cuda()
    model.train()

    # similarity loss definition
    loss_fn = torch.nn.CrossEntropyLoss()

    # scribble loss definition
    loss_fn_scr = torch.nn.CrossEntropyLoss()

    # continuity loss definition
    loss_hpy = torch.nn.L1Loss(reduction='mean')
    loss_hpz = torch.nn.L1Loss(reduction='mean')

    HPy_target = torch.zeros(im.shape[0] - 1, im.shape[1], nChannel)
    HPz_target = torch.zeros(im.shape[0], im.shape[1] - 1, nChannel)
    if use_cuda:
        HPy_target = HPy_target.cuda()
        HPz_target = HPz_target.cuda()

    if (cyclic):
        optimizer = optim.SGD(model.parameters(), lr=1., momentum=0.9)
        step_size = 32
        # step_size = 0.05
        clr = cyclical_lr(step_size, min_lr=0.01, max_lr=0.75)
        # clr = cyclical_lr(step_size, min_lr=0.00001, max_lr=0.01)
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, [clr])
    else:
        optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)

    label_colours = np.random.randint(255, size=(100, 3))

    for batch_idx in range(maxIter):
        # forwarding
        optimizer.zero_grad()
        output = model(data)[0]
        output = output.permute(1, 2, 0).contiguous().view(-1, nChannel)

        outputHP = output.reshape((im.shape[0], im.shape[1], nChannel))
        HPy = outputHP[1:, :, :] - outputHP[0:-1, :, :]
        HPz = outputHP[:, 1:, :] - outputHP[:, 0:-1, :]
        lhpy = loss_hpy(HPy, HPy_target)
        lhpz = loss_hpz(HPz, HPz_target)

        ignore, target = torch.max(output, 1)
        im_target = target.data.cpu().numpy()
        nLabels = len(np.unique(im_target))

        # loss
        if scribble:
            loss = stepsize_sim * loss_fn(output[inds_sim], target[inds_sim]) + stepsize_scr * loss_fn_scr(
                output[inds_scr], target_scr[inds_scr]) + stepsize_con * (lhpy + lhpz)
        else:
            loss = stepsize_sim * loss_fn(output, target) + stepsize_con * (lhpy + lhpz)

        loss.backward()

        optimizer.step()
        if (cyclic):
            scheduler.step()  # > Where the magic happens

        print(batch_idx, '/', maxIter, '|', ' label num :', nLabels, ' | loss :', loss.item(), ' | LR :',
              optimizer.param_groups[0]['lr'])

        if nLabels <= minLabels:
            print("nLabels", nLabels, "reached minLabels", minLabels, ".")
            break

    # save output image
    if not visualize:
        output = model(data)[0]
        output = output.permute(1, 2, 0).contiguous().view(-1, nChannel)
        ignore, target = torch.max(output, 1)
        im_target = target.data.cpu().numpy()
        # im_target_rgb = np.array([label_colours[ c % nChannel ] for c in im_target])
        # im_target_rgb = im_target_rgb.reshape((im.shape[0],im.shape[1],3)).astype( np.uint8 )
        les_unique_labels = np.unique(im_target)
        im_target_rgb = im_target.copy()
        for idx, ele in enumerate(les_unique_labels):
            # print("Idx: ", idx, " Label: ", ele)
            # Substitute values for idx from 0 to n_labels
            im_target_rgb[im_target_rgb == ele] = idx

        # im_target_rgb = np.array([label_colours[ c % nChannel ] for c in im_target for new_v in])
        im_target_rgb = im_target_rgb.reshape((im.shape[0], im.shape[1])).astype(np.uint8)

        if resize_output:
            im_target_rgb = cv2.resize(im_target_rgb, original_shape, 0, 0, interpolation=cv2.INTER_NEAREST)

    final_final_image = im_target_rgb.copy()
    for ele_i, ele in enumerate(np.unique(im_target_rgb)):
        final_final_image[final_final_image == ele] = ele_i

    return final_final_image
