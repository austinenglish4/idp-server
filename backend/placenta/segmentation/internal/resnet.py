import torch
import numpy as np
import pickle
from .utilities import segments_color_map
import cv2
from skimage.transform import resize
import os

PROJECT_SUB_DIRS = ['placenta', 'segmentation', 'internal']

def mask_from_JIT(spim, IMAGE_CHANNELS, torchscript_model_name, IMAGE_SIZE=128):
    full_model_path = os.path.join(
        PROJECT_SUB_DIRS[0], PROJECT_SUB_DIRS[1], PROJECT_SUB_DIRS[2], torchscript_model_name)
    
    net_loaded = torch.jit.load(full_model_path)
    # Check if there is GPU if not set trainning to CPU (very slow)
    device = torch.device('cpu')
    net_loaded.eval() # Set to evaluation mode

    # 1. original image
    #spim, wavelength, rgb_img, metadata = read_stiff(f"{REFLECTANCE_IMAGE_PATH}/{filename}") # spim(1024, 1024, 37)
    test_image_o = resize(spim,(IMAGE_SIZE,IMAGE_SIZE)) # spim for spectral image # test_image (128, 128, 37)
    test_image = np.expand_dims(test_image_o, axis=0) # (1, 128, 128, 37)

    # 3. predicted mask
    if IMAGE_CHANNELS == 3:
        ## use pca from training dataset
        pickle_file = 'placenta_principal_components.pickle'
        full_pickle_path = os.path.join(
            PROJECT_SUB_DIRS[0], PROJECT_SUB_DIRS[1], PROJECT_SUB_DIRS[2], pickle_file)
        
        with open(full_pickle_path, 'rb') as handle:
            pca_allinone = pickle.load(handle)

        #print(pca_allinone.components_)
        test_image_3_all = test_image_o.reshape(test_image_o.shape[0]*test_image_o.shape[1], -1) # 128*128, 37
        test_image_3_all = pca_allinone.transform(test_image_3_all) # 128*128, 3
        test_image_3_all = test_image_3_all.reshape(IMAGE_SIZE, IMAGE_SIZE, 3) # 128,128,3

        test_image_3_all = np.moveaxis(test_image_3_all, -1, 0)# 3 128 128
        test_image_3_all = torch.from_numpy(test_image_3_all.astype(np.float32))
        test_image_3_all = torch.autograd.Variable(test_image_3_all, requires_grad=False).to(device).unsqueeze(0) # 1  3 128 128

        with torch.no_grad():
            Prd = net_loaded(test_image_3_all)  # Run net
        #Prd = torchvision.transforms.Resize((height_orgin,widh_orgin))(Prd[0]) # Resize to origninal size

        seg = torch.argmax(Prd[0], 0).cpu().detach().numpy()  # Get  prediction classes
        pred_mask = np.expand_dims(np.squeeze(seg),axis=-1)


    final_mask = pred_mask.copy()
    for i,e in enumerate(np.unique(pred_mask)):
        final_mask[pred_mask == e] = i

    return final_mask


def start(spim,originalMask=[],IMAGE_CHANNELS=3,torchscript_model_name="modelJit2.ts", plot=False, resize_output=True):
    print("ResNet")

    estimated_mask = mask_from_JIT(spim, IMAGE_CHANNELS, torchscript_model_name)

    if resize_output:
        estimated_mask = cv2.resize(estimated_mask, spim.shape[0:2], 0, 0, interpolation = cv2.INTER_NEAREST)

    return estimated_mask