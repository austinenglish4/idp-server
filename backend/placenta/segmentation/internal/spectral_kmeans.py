import numpy as np
from sklearn.cluster import SpectralClustering
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics.pairwise import manhattan_distances
from .utilities import segments_color_map
import cv2

def rel_diff(x, y):
    return abs(x-y)/max(x,y)

def k_means(points, k=3, threshold=0.001, max_iters=100, dist_func='euclidean'):
    '''
        returns a numpy array with the centroid of each point at its index in the
        points array, and an array containing the centroids
    '''

    dist = {'euclidean': euclidean_distances, 'manhattan': manhattan_distances}[dist_func]
    # assume points are gaussian and pick k random points from the destribution as the centroids
    centroids = np.random.randn(k, points.shape[1])
    prev_loss = 1e9
    for i in range(max_iters):
        dists_to_centroids = dist(X=points, Y=centroids)
        assignment = np.argmin(dists_to_centroids, axis=1)
        loss = np.sum(np.linalg.norm(centroids[assignment] - points, axis=1) ** 2)
        if rel_diff(loss, prev_loss) <= threshold:
            break
        prev_loss = loss
        # update the centroids
        for cent_idx in range(k):
            cent_points = points[np.where(assignment == cent_idx)]
            if cent_points.shape[0] > 0:  # to protect against when no points belong to the centroid
                centroids[cent_idx] = np.mean(cent_points, axis=0)

    return assignment, centroids, loss

def k_means_rep(points, k=3, threshold=0.001, max_iters=100, dist_func='euclidean', reps=1, ):
    min_loss = 1e15
    best_centroids, best_assignment = None, None
    for rep in range(reps):
        assignment, centroids, loss = k_means(points=points, k=k, threshold=threshold, max_iters=max_iters, dist_func=dist_func)
        if loss <= min_loss:
            min_loss, best_centroids, best_assignment = loss, centroids, assignment
    return best_assignment, best_centroids, min_loss

def segment_images(img, n_clusters=8):

    #assignments = []
    #for k in [3, 5, 7, 9, 11]:
    pixel_map = img.reshape(img.shape[0] * img.shape[1], img.shape[2])
    assignment, _, _ = k_means_rep(points=pixel_map, k=n_clusters, reps=5)
    z = assignment.reshape((img.shape[0],img.shape[1]))
    #z = np.rot90(z, 2)
    #z = np.fliplr(z)

        #assignments.append(z)
    ass_array = np.asarray(z)
    return ass_array


def spectral_segment(img, n_clusters=5):

    cluster_idx =  SpectralClustering(n_clusters=n_clusters, affinity='nearest_neighbors',
                                      n_neighbors=n_clusters,
                                      n_jobs=-1, eigen_solver='arpack',verbose=False).fit_predict(img.reshape(-1,img.shape[2]))
    return cluster_idx.reshape(img.shape[0],img.shape[1])

def start(n_channel_img, method="complex", n_clusters=8, originalMask=[], title_sufix='', plot=False, resize_output=True,original_shape=() ):
    print("Spectral K-Means")
    if method == "simple":
        spectral_seg_img = segment_images(n_channel_img, n_clusters=n_clusters)
    elif method == "complex":
        spectral_seg_img = spectral_segment(n_channel_img, n_clusters=n_clusters)


    if resize_output:
        spectral_seg_img = cv2.resize(spectral_seg_img, original_shape, 0, 0, interpolation=cv2.INTER_NEAREST)

    return spectral_seg_img
