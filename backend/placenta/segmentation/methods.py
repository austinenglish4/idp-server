from enum import Enum
from placenta.models import SpectralImage
from .spectral_tiffs import *
import h5py
import numpy as np
from PIL import Image
from .internal.austin import *
from PIL import Image


class SEG_METHODS(Enum):
    KMEANS = 1
    ADVANCED_DL = 2
    SIMPLE_DL = 3
    RESNET = 4
    SPECTRAL_KMEANS = 5


def handler(spim: SpectralImage, run_fn):
    spimFilename = spim.path_to_hdf5
    with h5py.File(spimFilename, "r") as f:
        hsi = f['spim'][()]
        rgb = f['rgb'][()]
    labels = run_fn(hsi)
    masks = {}

    for i in np.unique(labels):
        mask = np.full(rgb.shape[:2], False)
        mask[labels == i] = True
        # mask = Image.fromarray(mask, 'L')
        masks[i] = mask
    return masks


def segmentation_method(spim: SpectralImage, method: SEG_METHODS):
    if method is SEG_METHODS.KMEANS.value:
        return handler(spim, run_normalkmeans)
    elif method is SEG_METHODS.ADVANCED_DL.value:
        return handler(spim, run_combination_advanced_dl)
    elif method is SEG_METHODS.SIMPLE_DL.value:
        return handler(spim, run_simple_dl)
    elif method is SEG_METHODS.RESNET.value:
        return handler(spim, run_resnet)
    elif method is SEG_METHODS.SPECTRAL_KMEANS.value:
        return handler(spim, run_spectral_kmeans)
    else:
        return []
