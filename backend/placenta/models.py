from email.policy import default
from django.db import models
import numpy as np
import pickle
from django.core import checks
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import pre_delete 
from django.dispatch import receiver
import shutil

class SpectralImage(models.Model):
    label               = models.CharField(max_length=50, null=True)
    path_to_hdf5        = models.FilePathField(null=False)
    path_to_png         = models.FilePathField(null=True)
    path_to_thumbnail   = models.FilePathField(null=True)
    spim_cube_metadata  = models.CharField(max_length=100, null=True)
    
    # Metadata descriptions
    patient_id                  = models.CharField(max_length=20, null=True)
    diagnosis                   = models.TextField(max_length=100, null=True)
    image_identifier            = models.CharField(max_length=20, null=True)
    image_series_identifier     = models.CharField(max_length=20, null=True)
    hsi_camera_name             = models.TextField(max_length=20, null=True)
    optic_imaging_system        = models.TextField(max_length=20, null=True)
    hsi_adapter_name            = models.TextField(max_length=20, null=True)
    environment_description     = models.TextField(max_length=20, null=True)
    reference                   = models.TextField(max_length=20, null=True)
    light_source_description    = models.TextField(max_length=20, null=True)
    hsi_software_name           = models.TextField(max_length=20, null=True)
    hsi_software_version        = models.TextField(max_length=20, null=True)
    acquisition_type            = models.TextField(max_length=20, null=True)
    hsi_sequence_name           = models.CharField(max_length=20, null=True)
    hsi_sequence_version        = models.CharField(max_length=20, null=True)
    timestamp                   = models.DateTimeField(null=True)
    data_size                   = models.IntegerField(null=True)
    frames                      = models.IntegerField(null=True)
    imaging_light_intensity     = models.FloatField(null=True)
    imaging_magnification       = models.FloatField(null=True)
    imaging_focal_length        = models.FloatField(null=True)
    notes                       = models.TextField(max_length=100, null=True)

class Annotation(models.Model):
    spim_id         = models.OneToOneField(SpectralImage, on_delete=models.CASCADE, primary_key=True)
    label           = models.CharField(max_length=50, null=True)
    
    # postprocessing metadata
    annotation_file_type        = models.TextField(max_length=20, null=True)
    label_library_name          = models.TextField(max_length=20, null=True)
    label_library_version       = models.CharField(max_length=20, null=True)
    reference_rgb_image_id      = models.CharField(max_length=20, null=True)
    tissue_classes              = models.IntegerField(null=True)
    class_features              = models.IntegerField(null=True)
    annotation_rate             = models.FloatField(null=True)
    description_rate            = models.FloatField(null=True)
    notes                       = models.TextField(max_length=100, null=True)

class Diagnosis(models.Model):
    diagnosis_type  = models.CharField(max_length=50, null=True)
    level           = models.CharField(max_length=50, null=True)
    description     = models.CharField(max_length=50, null=True)

class ClassFeature(models.Model):
    class_feature   = models.CharField(max_length=50, null=True)

class TissueClass(models.Model):
    tissue_type     = models.CharField(max_length=50, null=True)
    class_id        = models.ForeignKey(ClassFeature, null=True, on_delete=models.PROTECT)

class IndividualMask(models.Model):
    annotation_id   = models.ForeignKey(Annotation, on_delete=models.CASCADE)
    label           = models.CharField(max_length=50, null=True)
    path_to_png     = models.FilePathField(null=False)
    polygon_data    = models.TextField(null=True)
    color_hex       = models.CharField(null=True, max_length=6)
    tissue_id       = models.ForeignKey(TissueClass, null=True, on_delete=models.PROTECT)
    diagnosis_id    = models.ForeignKey(Diagnosis, null=True, on_delete=models.PROTECT)

##################################
# Legacy, to be removed
class Mask(models.Model):
    spim_id     = models.OneToOneField(SpectralImage, on_delete=models.CASCADE, primary_key=True)
    file_name   = models.CharField(max_length=50)
    csv_data    = models.TextField(null=False)
    set_name    = models.CharField(max_length=25, null=True)
##################################
