
"""

For initializing the database.
'python manage.py initialize' to run

"""

from django.core.management.base import BaseCommand, CommandError
import os
import random
import json
import string
import pathlib
import h5py
import numpy as np
import imageio
from PIL import Image as im
from datetime import datetime, timezone
from django.shortcuts import redirect, render
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from placenta.models import SpectralImage, Mask, Annotation, IndividualMask, TissueClass, ClassFeature, Diagnosis
from utils.annotations import *
from utils.spectral_tiffs import *
from dotenv import load_dotenv
import uuid
import glob
from placenta.segmentation.methods import *
import re


def create_mask_instance(flattened, filename, polygon_masks=None, spim=None):

    if filename[-4:] == '.csv':  # from csv
        filename = filename[:-4]
    else:  # from tif
        filename = filename[:-10]

    # Get reference to spim object via label
    if spim is not None:
        spectral_image = spim
    else:
        spectral_image = SpectralImage.objects.get(label=filename)

    # Get reference to Annotation object
    annotation = Annotation.objects.get(spim_id=spectral_image)

    # If masks were created from the other format (csv or tif), remove the old one
    if IndividualMask.objects.filter(annotation_id=annotation).exists():
        masks = IndividualMask.objects.filter(annotation_id=annotation)
        for m in masks:
            files = glob.glob('./placenta/' + m.path_to_png)
            for f in files:
                os.remove(f)
        IndividualMask.objects.filter(annotation_id=annotation).delete()

    # Get reference to ClassFeature & TissueClass. This is dummy data
    class_feature = ClassFeature.objects.get(class_feature="Class feature #1")
    tissue_class = TissueClass.objects.get(class_id=class_feature)

    i = 0
    # For each mask in annotation
    for mask in flattened:
        # Create png of the mask
        png = np.reshape(flattened[mask], (
            flattened[mask].shape[0],
            flattened[mask].shape[1]
        ))
        png_data = im.fromarray(png).convert('L')
        rgba = png_data
        rgba.putalpha(im.fromarray(png))
        png_path = './placenta/files/masks/' + str(uuid.uuid4()) + '.png'
        png_data.save(png_path)

        # Create mask object
        indmask = IndividualMask.objects.create(
            annotation_id=annotation,
            path_to_png=png_path[10:],
            label=str(mask),
            color_hex='%02x%02x%02x' % (random.randint(
                0, 255), random.randint(0, 255), random.randint(0, 255)),
            tissue_id=tissue_class
        )
        if polygon_masks:
            # Get color value from csv polygon data
            rgb = polygon_masks[i].split(';', 1)[1].split(';', 1)[1].split(';', 1)[
                0].replace(' ', '').split(',', 2)
            indmask.color_hex = '%02x%02x%02x' % (
                int(rgb[0]), int(rgb[1]), int(rgb[2]))
            indmask.polygon_data = polygon_masks[i]
        i += 1


class Command(BaseCommand):

    def handle(self, *args, **options):

        load_dotenv()

        # Delete all DB data
        SpectralImage.objects.all().delete()
        Mask.objects.all().delete()
        Annotation.objects.all().delete()
        IndividualMask.objects.all().delete()
        TissueClass.objects.all().delete()
        ClassFeature.objects.all().delete()
        Diagnosis.objects.all().delete()

        # Create dummy data for TissueClass, ClassFeature, Diagnosis
        for i in range(1, 6):
            cf = ClassFeature.objects.create(
                class_feature='Class feature #' + str(i)
            )
            TissueClass.objects.create(
                tissue_type='Tissue type #' + str(i),
                class_id=cf
            )
            Diagnosis.objects.create(
                diagnosis_type='Diagnosis type #' + str(i),
                level='Diagnosis #' + str(i) + ' level',
                description='Diagnosis #' + str(i) + ' description'
            )

        # Directory to read input data
        """
        backend                 (project root)
            -backend            (django)
            -Placenta HSI       (input data)
                -data
                    -binary-masks
                    -flat-fielded
                    -Set #1 Spimlab-annotations
        """
        try:
             localdir = pathlib.Path(os.getenv('INPUT_DATA_FOLDER')) # read from .env file
        except:
             raise ValueError('Could not resolve hsi directory provided in INPUT_DATA_FOLDER: ' + os.getenv('INPUT_DATA_FOLDER'))
        print(str(localdir))
        # localdir = pathlib.Path('..').joinpath('Placenta HSI')

        dir1 = localdir.joinpath('data').joinpath('flat-fielded')
        # dir2 = localdir.joinpath('data').joinpath('Set #1 Spimlab-annotations')
        dir3 = localdir.joinpath('data').joinpath('binary-masks')

        dirlist = [
            './placenta/files/hdf5_files/*',
            './placenta/files/masks/*',
            './placenta/files/rgb/*',
            './placenta/files/temp/*',
            './placenta/files/thumbnails/*'
        ]
        for d in dirlist:
            files = glob.glob(d)
            for f in files:
                os.remove(f)

        # Create SpectralImage objects
        directory = localdir.joinpath(dir1)
        for filename in [f for f in os.listdir(directory) if re.match(r'.*(\.tif$|\.tiff$)', f)]:
            file = directory.joinpath(filename)
            print(f'Loading file {file} into database')
            
            if pathlib.Path.is_file(file):

                # Read data and size from file
                time = os.path.getctime(file)
                time = datetime.fromtimestamp(time, timezone.utc)
                time = time.astimezone().isoformat()
                size = os.path.getsize(file)

                # Get spim cube
                spim_tuple = read_stiff(file)

                # Write to hdf5 file
                hdf5_path = 'placenta/files/hdf5_files/' + \
                    str(uuid.uuid4()) + '.hdf5'
                with h5py.File(hdf5_path, 'w') as f:
                    f.create_dataset(
                        'spim', spim_tuple[0].shape, data=spim_tuple[0])
                    f.create_dataset(
                        'wavelengths', spim_tuple[1].shape, data=spim_tuple[1])
                    f.create_dataset(
                        'rgb', spim_tuple[2].shape, data=spim_tuple[2])

                # Create thumbnail png's
                tn = np.reshape(spim_tuple[2], (
                    spim_tuple[2].shape[0],  # 1024
                    spim_tuple[2].shape[1],  # 1024
                    spim_tuple[2].shape[2]  # 3
                ))

                # Create thumbnail of rgb image
                tn_data = im.fromarray(tn)
                png_path = './placenta/files/rgb/' + str(uuid.uuid4()) + '.png'
                tn_data.save(png_path)
                tn_data = tn_data.resize((128, 128), im.ANTIALIAS)
                tn_path = './placenta/files/thumbnails/' + \
                    str(uuid.uuid4()) + '.png'
                tn_data.save(tn_path)

                # Create spim object
                spim = SpectralImage.objects.create(
                    label=filename[:-4],
                    path_to_hdf5=hdf5_path,
                    path_to_png=png_path[10:],
                    path_to_thumbnail=tn_path[10:],
                    spim_cube_metadata=spim_tuple[3],

                    patient_id=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    diagnosis=(''.join(random.choice(
                        string.ascii_letters + ' ') for i in range(25))),
                    image_identifier=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    image_series_identifier=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    hsi_camera_name='Senop HSC-2',
                    optic_imaging_system='Pentero 900',
                    hsi_adapter_name=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    environment_description=(
                        ['OR301', 'OR302', 'OR303'][random.randint(0, 2)]),
                    reference=(['reflectance', 'absobance']
                               [random.randint(0, 1)]),
                    light_source_description=(
                        ['Xenon', 'Blue400', 'Yellow500'][random.randint(0, 2)]),
                    hsi_software_name='Senop',
                    hsi_software_version=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    acquisition_type=(['snapshot', 'video']
                                      [random.randint(0, 1)]),
                    hsi_sequence_name=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    hsi_sequence_version=(''.join(random.choice(
                        string.ascii_letters + string.digits) for i in range(10))),
                    timestamp=time,
                    data_size=size,
                    frames=spim_tuple[1].shape[0],
                    imaging_light_intensity=random.random(),
                    imaging_magnification=random.random(),
                    imaging_focal_length=random.random(),
                    notes=(''.join(random.choice(string.ascii_letters + '')
                           for i in range(50))),
                )

                # Initialize Annotation instance for created spim
                Annotation.objects.create(
                    label=filename[:-4],
                    spim_id=spim
                )

                # Create Mask Objects from segmentation
                # masks = segmentation_method(SEG_METHODS.KMEANS.value, spim)
                # create_mask_instance(masks, f'{spim.label}_mask', spim=spim)

        # Create Mask objects from binary mask files
        directory = localdir.joinpath(dir3)
        for filename in [f for f in os.listdir(directory) if re.match(r'.*(\.tif$|\.tiff$)', f)]:
            file = os.path.join(directory, filename)
            print(f'Loading file {file} into database')
            
            if os.path.isfile(file):

                flattened = read_mtiff(file)
                try:
                    create_mask_instance(flattened, filename)
                except SpectralImage.DoesNotExist:
                    print(f'Could not create mask {filename}')

        # Create Mask objects from CSV files
        # directory = localdir.joinpath(dir2)
        # for filename in os.listdir(dir2):
        #     file = directory.joinpath(filename)
        #     if pathlib.Path.is_file(file):

        #         with open(file, 'r') as f:
        #             csv_data = f.read()

        #         # Seperate individual masks from csv polygon data
        #         lines = csv_data.split("\n")
        #         labels = list()
        #         for line in lines:
        #             label = ''
        #             for c in line:
        #                 if c == ';':
        #                     labels.append(label)
        #                     break
        #                 label += c
        #         labels = list(set(labels))
        #         polygon_masks = list()

        #         for label in labels:
        #             polygons = ''
        #             for line in lines:
        #                 if line[:len(label)] == label:
        #                     polygons += line
        #             polygon_masks.append(polygons)

        #         # create Mask object (old legacy model, to be deleted)
        #         spectral_image = SpectralImage.objects.get(label=filename[:-4])
        #         Mask.objects.create(
        #             spim_id=spectral_image,
        #             file_name=filename[:-4],
        #             csv_data=csv_data,
        #             set_name=filename[0:5]
        #         )

        #         parsed = parse(csv_data)
        #         # TODO: remove hardcoded values (how?)
        #         masks = create_masks(parsed, 1024, 1024)
        #         flattened = flatten_masks(masks)
        #         create_mask_instance(flattened, filename, polygon_masks)

        self.stdout.write(self.style.SUCCESS('Success'))
