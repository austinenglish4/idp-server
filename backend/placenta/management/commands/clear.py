
"""

For clearing the database.
'python manage.py clear' to run

"""

from django.core.management.base import BaseCommand, CommandError
from placenta.models import SpectralImage, Mask, Annotation, IndividualMask, TissueClass, ClassFeature, Diagnosis

class Command(BaseCommand):

    def handle(self, *args, **options):
        
        SpectralImage.objects.all().delete()
        Mask.objects.all().delete()
        Annotation.objects.all().delete()
        IndividualMask.objects.all().delete()
        TissueClass.objects.all().delete()
        ClassFeature.objects.all().delete()
        Diagnosis.objects.all().delete()
            
        self.stdout.write(self.style.SUCCESS('Success'))

