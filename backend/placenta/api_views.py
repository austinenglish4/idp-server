import os
import random
import json
import string
import pathlib
import h5py
import numpy as np
import imageio
from PIL import Image as im
from datetime import datetime, timezone
from django.shortcuts import redirect, render
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from django.http import HttpResponse
from django.http import FileResponse
from django.http import JsonResponse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from placenta.models import SpectralImage, Mask, Annotation, IndividualMask, TissueClass, ClassFeature, Diagnosis
from .forms import UploadFileForm
from utils.annotations import *
from utils.spectral_tiffs import *
import uuid
# from placenta.segmentation.AustinPort.austin import *
from placenta.segmentation.methods import SEG_METHODS, segmentation_method

"""
Functions for shared API
"""


@api_view(('GET',))
def get_spim(request, pk):
    """
    Get spim cube and masks
    """

    spim = SpectralImage.objects.get(pk=pk)
    annotation = Annotation.objects.get(spim_id=spim)
    masks = IndividualMask.objects.filter(annotation_id=annotation)

    mask_array = []
    for m in masks:
        mask_object = {
            'pk': m.pk,
            'bitmap': m.path_to_png
        }
        if m.label:
            mask_object['name'] = m.label

        try:
            if TissueClass.objects.filter(pk=m.tissue_id.pk).exists():
                tissue = TissueClass.objects.get(pk=m.tissue_id.pk)
                mask_object['tissueId'] = tissue.pk
            if ClassFeature.objects.filter(pk=tissue.class_id.pk).exists():
                mask_object['classId'] = ClassFeature.objects.get(
                    pk=tissue.class_id.pk).pk
        except:
            print()

        mask_array.append(mask_object)

    ann = Annotation.objects.get(spim_id=spim)

    response = {
        'name': spim.label,
        'pk': spim.pk,
        'rgb': spim.path_to_png,
        'spim_cube': spim.path_to_hdf5,
        'annotation_id': ann.pk,
        'masks': mask_array
    }

    return Response(response)

@api_view(('DELETE',))
def delete_spim(request, pk):
    """
    Get spim cube and masks
    """

    print(pk)
    try:
        spim = SpectralImage.objects.get(pk=pk)
        spim.delete()
        
    except:
        print('Could not find spim id: ' + pk)
    

    response = {
        'ok': True
    }

    return Response(response)


@api_view(['GET'])
def get_spims(request):
    """
    Get spim labels, pk, rgb render
    """

    spims = SpectralImage.objects.all()
    spim_array = []
    for s in spims:
        annotation = Annotation.objects.get(spim_id=s.pk)
        try:
            masks = IndividualMask.objects.filter(annotation_id=annotation)
        except IndividualMask.DoesNotExist:
            masks = []

        spim_object = {
            'name': s.label,
            'pk': s.pk,
            'rgb': s.path_to_png,
            'masks': [{'pk': m.pk, 'bitmap': m.path_to_png, 'tissueId': m.tissue_id.pk} for m in masks]
        }
        spim_array.append(spim_object)

    response = {
        'spims': spim_array
    }

    return Response(response)


@api_view(['GET', 'POST', 'DELETE'])
def update_mask(request, pk, label=None):
    """
    Update specific mask
    """

    if request.method == 'POST':

        # Get reference to Mask object
        mask = IndividualMask.objects.get(pk=pk)

        if label is not None:
            mask.label = label
        # Create png of the mask in payload
        png_path = mask.path_to_png.split('/files/')[1]
        with default_storage.open(png_path, 'wb+') as binary_file:
            binary_file.write(request.body)

        mask.save()

        response = {
            'pk': mask.pk,
            'label': mask.label,
            'annotation_id': mask.annotation_id.pk,
            'path_to_png': mask.path_to_png,
            'tissue_id': mask.tissue_id.pk
        }

        return Response(response)

    if request.method == 'DELETE':
        # Get reference to Mask object
        mask = IndividualMask.objects.get(pk=pk)
        mask.delete()
        return Response('200 OK')

    return HttpResponse(status=400)


@api_view(['GET', 'POST'])
def upload_mask(request, pk, label=None):
    """
    Upload new mask for specific spim
    """

    if request.method == 'POST':

        # Create png of the mask in payload
        png_path = 'masks/' + str(uuid.uuid4()) + '.png'
        with default_storage.open(png_path, 'wb+') as binary_file:
            binary_file.write(request.body)

        png_path = f'files/{png_path}'

        ann = Annotation.objects.get(spim_id=pk)
        cf = ClassFeature.objects.create()
        cf.class_feature = f'Class feature {cf.pk}'
        cf.save()
        tissue = TissueClass.objects.create(class_id=cf)
        tissue.tissue_type = f'Tissue type {tissue.pk}'
        tissue.save()

        indmask = IndividualMask.objects.create(
            annotation_id=ann,
            path_to_png=png_path,
            tissue_id=tissue
        )
        if label:
            indmask.label = label

        response = {
            'pk': indmask.pk,
            'label': indmask.label,
            'annotation_id': indmask.annotation_id.pk,
            'path_to_png': indmask.path_to_png,
            'tissue_id': indmask.tissue_id.pk
        }

        return Response(response)

    return HttpResponse(status=400)


@api_view(['GET', 'POST'])
def segmentation_masks(request, pk, seg_type=SEG_METHODS.KMEANS.value):
    """
    Create masks from automatic segmentation
    """
    try:
        seg_type = int(seg_type)
    except:
        raise ValueError(f'Invalid segmentation method type "{seg_type}"')

    if request.method == 'POST':

        spim = SpectralImage.objects.get(pk=pk)
        hdf5_path = spim.path_to_hdf5
        with h5py.File(hdf5_path, 'r') as f:
            masks = segmentation_method(spim, seg_type)
            ann = Annotation.objects.get(spim_id=spim)

            IndividualMask.objects.filter(annotation_id=ann).delete()
            for idx, m in enumerate(masks):
                mask = masks[m]
                png_data = im.fromarray(mask).convert('L')
                rgba = png_data
                rgba.putalpha(im.fromarray(mask))
                png_path = './placenta/files/masks/' + \
                    str(uuid.uuid4()) + '.png'
                png_data.save(png_path)

                cf = ClassFeature.objects.create()
                cf.class_feature = f'Class feature {cf.pk}'
                tissue = TissueClass.objects.create(class_id=cf)
                tissue.tissue_type = f'Tissue type {tissue.pk}'
                tissue.save()
                IndividualMask.objects.create(
                    annotation_id=ann,
                    path_to_png=png_path[10:],
                    label=f'{SEG_METHODS(seg_type).name} {idx}',
                    tissue_id=tissue
                )

        masks = IndividualMask.objects.filter(annotation_id=ann)
        response = {
            'spim pk': spim.pk,
            'annotation pk': ann.pk,
            'masks': [
                {
                    'pk': mask.pk,
                    'label': mask.label,
                    'annotation_id': mask.annotation_id.pk,
                    'bitmap': mask.path_to_png,
                    'tissue_id': mask.tissue_id.pk
                } for mask in masks
            ]
        }

        return Response(response)

    return HttpResponse(status=400)


@api_view(['GET'])
def get_tissueclasses(request):
    """
    Get all TissueClasses
    """

    classes = TissueClass.objects.all()
    tc_array = []
    for i in classes:
        try:
            class_id = i.class_id.pk
        except:
            class_id = None

        tc_array.append({
            'tissue_type': i.tissue_type,
            'class_id': class_id,
            'pk': i.pk
        })

    response = {
        'TissueClass': tc_array
    }

    return Response(response)


@api_view(['GET'])
def get_classfeatures(request):
    """
    Get all ClassFeatures
    """

    features = ClassFeature.objects.all()
    cf_array = []
    for i in features:
        cf_array.append({
            'class_feature': i.class_feature,
            'pk': i.pk
        })

    response = {
        'ClassFeature': cf_array
    }

    return Response(response)


@api_view(['GET'])
def get_diagnoses(request):
    """
    Get all Diagnoses
    """

    diagnoses = Diagnosis.objects.all()
    d_array = []
    for i in diagnoses:
        d_array.append({
            'diagnosis_type': i.diagnosis_type,
            'level': i.level,
            'description': i.description,
            'pk': i.pk,
        })

    response = {
        'Diagnosis': d_array
    }

    return Response(response)
