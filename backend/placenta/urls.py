from django.urls import path
from . import views
from . import api_views
from django.conf import settings
from django.conf.urls.static import static
from django.shortcuts import redirect, render
from django.urls import re_path
from django.views.generic import TemplateView

def redirectToAppIndex(request, arg=None):
     if 'files' not in request.GET:
          return render(request, 'index.html')

urlpatterns = [

    path('api/spim/<str:pk>', api_views.get_spim, name='api-spim'),
    path('api/spim/<str:pk>/delete', api_views.delete_spim, name='api-spim'),
    path('api/spims', api_views.get_spims, name='api-spims'),
    path('api/tissueclass', api_views.get_tissueclasses, name='api-tissueclass'),
    path('api/classfeature', api_views.get_classfeatures, name='api-classfeature'),
    path('api/diagnosis', api_views.get_diagnoses, name='api-diagnosis'),
    path('api/spim/<str:pk>/autoseg/<str:seg_type>',
         api_views.segmentation_masks, name='api-autoseg'),
    path('api/spim/<str:pk>/autoseg',
         api_views.segmentation_masks, name='api-autoseg'),
    path('api/spim/<str:pk>/createmask/<str:label>',
         api_views.upload_mask, name='api-upload-mask'),
    path('api/spim/<str:pk>/createmask',
         api_views.upload_mask, name='api-upload-mask'),
    path('api/mask/<str:pk>/update/<str:label>',
         api_views.update_mask, name='api-update-mask'),
    path('api/mask/<str:pk>/update', api_views.update_mask, name='api-update-mask'),
    path('api/mask/<str:pk>/delete', api_views.update_mask, name='api-update-mask'),

    path('mask/<str:pk>/create', views.create_individual_mask,
         name='create-ind-mask'),
    path('mask/<str:pk>/update', views.update_individual_mask,
         name='update-ind-mask'),
    path('mask/<str:pk>/delete', views.delete_individual_mask,
         name='delete-ind-mask'),

    path('spim/<str:pk>/bands/<str:index>/', views.get_band, name='band'),
    path('spim/<str:pk>/update', views.update_spim_metadata, name='update-spim'),
    path('spim/<str:pk>/metadata', views.metadata_page, name='metadata-page'),
    path('spim/<str:pk>/upload_annotation',
         views.upload_annotation, name='upload-annotation'),
    path('spim/<str:pk>/delete_annotation',
         views.delete_annotation, name='delete-annotation'),
    path('spim/<str:pk>/', views.spectral_image, name='spim'),

    path('spims/', views.spectral_images, name='spims'),
    path('spimslist/', views.spectral_images_list, name='spimslist'),
    re_path('browse/', redirectToAppIndex),
    re_path('annotate/<str:id>', redirectToAppIndex),
    re_path(r'^((?!files).)*$', redirectToAppIndex),
     # path('', views.home, name='home')
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
