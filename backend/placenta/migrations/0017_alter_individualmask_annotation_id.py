# Generated by Django 4.1.1 on 2022-10-25 08:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('placenta', '0016_alter_annotation_spim_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='individualmask',
            name='annotation_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='placenta.annotation'),
        ),
    ]
