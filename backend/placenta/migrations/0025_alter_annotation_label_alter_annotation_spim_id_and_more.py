# Generated by Django 4.1.1 on 2022-11-05 18:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('placenta', '0024_alter_spectralimage_path_to_png'),
    ]

    operations = [
        migrations.AlterField(
            model_name='annotation',
            name='label',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='annotation',
            name='spim_id',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='placenta.spectralimage'),
        ),
        migrations.AlterField(
            model_name='individualmask',
            name='path_to_png',
            field=models.FilePathField(),
        ),
    ]
