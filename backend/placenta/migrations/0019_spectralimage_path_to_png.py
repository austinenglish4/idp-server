# Generated by Django 4.1.1 on 2022-10-26 07:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('placenta', '0018_individualmask_png_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='spectralimage',
            name='path_to_png',
            field=models.FilePathField(default=''),
        ),
    ]
