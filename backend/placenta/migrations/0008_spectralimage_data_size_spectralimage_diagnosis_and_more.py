# Generated by Django 4.1.1 on 2022-10-13 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('placenta', '0007_alter_spectralimage_path_to_thumbnail'),
    ]

    operations = [
        migrations.AddField(
            model_name='spectralimage',
            name='data_size',
            field=models.FloatField(default=None),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='diagnosis',
            field=models.TextField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='environment_description',
            field=models.TextField(default='OR301', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='frames',
            field=models.IntegerField(default=None),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='hsi_adapter_name',
            field=models.TextField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='hsi_camera_name',
            field=models.TextField(default='Senop HSC-2', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='hsi_sequence_name',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='hsi_sequence_version',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='image_identifier',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='image_series_identifier',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='imaging_focal_length',
            field=models.FloatField(default=None),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='imaging_light_intensity',
            field=models.FloatField(default=None),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='imaging_magnification',
            field=models.FloatField(default=None),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='light_source_description',
            field=models.TextField(default='Xenon', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='notes',
            field=models.TextField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='optic_imaging_system',
            field=models.TextField(default='Pentero 900', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='patient_id',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='reference',
            field=models.TextField(default='reflectance', max_length=20),
        ),
        migrations.AddField(
            model_name='spectralimage',
            name='timestamp',
            field=models.DateTimeField(default=None),
        ),
    ]
