# Generated by Django 4.1.1 on 2022-10-28 12:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('placenta', '0020_classfeature_diagnosis_tissueclass_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='annotation',
            name='annotation_file_type',
            field=models.TextField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='annotation_rate',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='class_features',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='description_rate',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='label_library_name',
            field=models.TextField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='label_library_version',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='notes',
            field=models.TextField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='reference_rgb_image_id',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='annotation',
            name='tissue_classes',
            field=models.IntegerField(null=True),
        ),
    ]
