from django.contrib import admin

# Register your models here.

from .models import SpectralImage, Mask, Annotation, IndividualMask, TissueClass, ClassFeature, Diagnosis

admin.site.register(SpectralImage)
admin.site.register(Mask)
admin.site.register(Annotation)
admin.site.register(IndividualMask)
admin.site.register(TissueClass)
admin.site.register(ClassFeature)
admin.site.register(Diagnosis)

